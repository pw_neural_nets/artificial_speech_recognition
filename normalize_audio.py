

import numpy as np
import librosa
import librosa.display
import os
import csv
import matplotlib.pyplot as plt
import random

import config

train_dir = config.data_path / "train"


def split_arr(arr):
    """
    split an array into chunks of length 16000
    Returns:
        list of arrays
    """
    return np.split(arr, np.arange(16000, len(arr), 16000))


def create_silence():
    """
    reads wav files in background noises folder,
    splits them and saves to silence folder in train_dir
    """
    for file in os.listdir('data/train/_background_noise_/'):
        if 'wav' in file:
            sig, rate = librosa.load('data/train/_background_noise_/' + file, sr=16000)
            sig_arr = split_arr(sig)
            if not os.path.exists(train_dir / 'silence/'):
                os.makedirs(train_dir + 'silence/')
            for ind, arr in enumerate(sig_arr):
                filename = 'frag%d' % ind + '_%s' % file  # example: frag0_running_tap.wav
                librosa.output.write_wav(train_dir / 'silence/' + filename, arr, 16000)


x, r = librosa.load(train_dir / 'yes/bfdb9801_nohash_0.wav', sr = 16000)
print('min: ',np.min(x),
      '\nmax: ', np.max(x),
      '\nmean: ', np.mean(x),
      '\nmedian: ', np.median(x),
      '\nvariance: ', np.var(x),
      '\nlength: ', len(x))
plt.plot(x)
plt.show()


def make_spec(file, file_dir=train_dir, flip=False, ps=False, st=4):
    """
    create a melspectrogram from the amplitude of the sound

    Args:
        file (str): filename
        file_dir (str): directory path
        flip (bool): reverse time axis
        ps (bool): pitch shift
        st (int): half-note steps for pitch shift
    Returns:
        np.array with shape (122,85) (time, freq)
    """
    sig, rate = librosa.load((file_dir/file), sr=16000)
    if len(sig) < 16000:  # pad shorter than 1 sec audio with ramp to zero
        sig = np.pad(sig, (0, 16000 - len(sig)), 'linear_ramp')
    if ps:
        sig = librosa.effects.pitch_shift(sig, rate, st)

    D = librosa.amplitude_to_db(librosa.stft(sig[:16000], n_fft=512,
                                             hop_length=128,
                                             center=False), ref=np.max)
    S = librosa.feature.melspectrogram(S=D, n_mels=85).T
    if flip:
        S = np.flipud(S)
    return S.astype(np.float32)


librosa.display.specshow(make_spec('yes/bfdb9801_nohash_0.wav'),
                         x_axis='mel',
                         fmax=8000,
                         y_axis='time',
                         sr=16000,
                         hop_length=128)

make_spec('yes/bfdb9801_nohash_0.wav').shape
plt.show()

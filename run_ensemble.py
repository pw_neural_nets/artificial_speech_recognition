from artifical_speech_recognition.utils import utilities
from artifical_speech_recognition.pipeline import ensemble_pipeline

utilities.set_random_seed()
# pipeline = ensemble_pipeline.WeightedVoter.from_model()
# pipeline = ensemble_pipeline.RFBlender.from_model()
pipeline = ensemble_pipeline.CatBlender.from_model()
pipeline.fit()
pipeline.score()

submission_preds = pipeline.predict_submission()
submission_preds.to_csv("kaggle_submission_ensamble.csv", index=False)

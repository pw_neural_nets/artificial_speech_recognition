import os
import pathlib

SEED = 420
log = True

## Data loading
dataset_name = 'MelSpeechCommands'
wav_model = "1d2d" # 1d, 1d2d
normalize_wav = False
dataset_types = ['train_ds', 'ordered_train_ds', 'valid_ds', 'test_ds']
label_mapping_type = "unk"
root = os.path.dirname(os.path.abspath(__file__))
loader_workers = 2


print(root)
# kaggle
if "kaggle" in root:
    data_path = pathlib.Path("/kaggle/input")
    load_checkpoint_model_path = "/kaggle/input/deepspeech2-model/model.pytorch"
    save_model_checkpoint_path = "/kaggle/working/model.pytorch"
    submission_wavs_root = "/kaggle/input/"
# local
else:
    lroot = pathlib.Path(__file__).parents[0]
    common_path = lroot/"data"
    data_path = common_path/"datasets"
    load_checkpoint_model_path = common_path/"model_checkpoints/small_ds/label_mix_bidir/model.pytorch"
    save_model_checkpoint_path = common_path/"model_checkpoints/model.pytorch"
    submission_wavs_root = data_path/"submission_wavs"

    # ensembles
    ens_train_sets = ["train", "test", "valid"]
    preds_path = pathlib.Path(common_path/"predictions/small_ds")
    predictions_paths = [preds_path/x for x in [
        "train-preds_bidir.csv",
        "train-preds_label-mix-bidir.csv",
        # "train-preds_whitenoise.csv",
        "train-preds_wav1d2d.csv",

    ]]
    submission_preds_paths = [preds_path/x for x in [
        "submission-preds_bidir.csv",
        "submission-preds_label-mix-bidir.csv",
        # "submission-preds_whitenoise.csv",
        "submission-preds_wav1d2d.csv",
    ]]


submission_wavs_folders = ["speechcommandsp1/audio1", "speechcommandsp2/audio2", "speechcommandsp3/audio3"]

dataset_parameters = {
    "MelSpeechCommands": {
        'sample_rate': 16000,
        'mean': -1.2936,
        'folders': [
            "smallmeltraincompeitionspeechcommandspart1",
            "smallmeltraincompeitionspeechcommandspart2",
            # 'melspeechcommandsv002part1',
            # 'melspeechcommandsv002part2',
        ],
        'valid_split_path': "testing_list.txt",
        'test_split_path': "validation_list.txt",
        'noise_file_path': [
            "_background_noise_/white_noise.wav",
            # "_background_noise_/running_tap.wav",
            # "_background_noise_/pink_noise.wav",
            # "_background_noise_/exercise_bike.wav",
            # "_background_noise_/dude_miaowing.wav",
            # "_background_noise_/doing_the_dishes.wav",
        ],
        'suffix': ".mel_spec",
    },
    "WavSpeechCommands": {
        'sample_rate': 16000,
        'mean': -39.9020,
        'folders': [
            "trainspeechcommands",
            # 'wavspeechcommandsv002part1',
            # 'wavspeechcommandsv002part2',
        ],
        'valid_split_path': "testing_list.txt",
        'test_split_path': "validation_list.txt",
        'noise_file_path': [
            "_background_noise_/white_noise.wav",
            # "_background_noise_/running_tap.wav",
            # "_background_noise_/pink_noise.wav",
            # "_background_noise_/exercise_bike.wav",
            # "_background_noise_/dude_miaowing.wav",
            # "_background_noise_/doing_the_dishes.wav",
        ],
        'suffix': ".wav",
    },
    "LJSpeech": None,
    "LibreSpeech": None,
}

# logging
example_interval = 100

## Audio
sample_rate = 16000
# might need to pad wavs to one sec
n_fft = 512
# window_size = 0.02
# window_stride = window_size / 2
hop_length = 128
n_mels = 85
min_padded_spectrogram_length = 0

max_mel_length = 126
max_wav_length = 16000

### Augmentation
label_mix = False
label_mix_prob = 0.6

augmentation_probability = 0.8

superimpose_noise = False
max_noise_level = 0.2
noise_signal_splits = 20
num_noise_samples = 400
silent_subclass_samples = 80 # number of generated silent samples
silent_samples_repetition = 3 # silent subclass exmaples * this

zeroing_proportion = 0.05

freq_mask_max_len = 10
time_mask_max_len = 5
spec_mean = 0
spec_std = 1
time_warping_para = 10

## Generic model
epochs = 120
lr = 1e-3
bs = 256
TTA = False
TTA_iterations = 3
max_truncated_relu = 20
bidirectional = True
lstm_layers = 2

fc_dropout = 0.0
attn_dropout = 0.0
conv_dropout = 0.0
rnn_dropout = 0.0

# text
special_symbols = ['~', '<unk>', '_'] # <pad>, <unk>, <blank>, | optional token for repated character
pad_idx = 0
unk_idx = 1
blank_idx = 2

all_labels = ["on", "right", "sheila", "stop", "tree", "up", "wow", "zero", "one", "seven", "six", "three", "two",
              "visual", "yes", "dog", "forward", "learn", "no", "backward", "down", "four", "left", "off", "bed",
              "eight", "go", "bird", "five", "happy", "marvin", "cat", "follow", "house", "nine", "silence", "unknown"]

target_labels = ["yes", "no", "up", "down", "left", "right", "on", "off", "stop", "go", "silence", "unknown"]

if label_mapping_type == "unk":
    label_mapping = target_labels
else:
    label_mapping = all_labels

# DeepSpeech
ds_in_featuers = 10
ds_hidden_fc = 10
ds_dropout = 0.2

# DeepSpeech2
channels_shapes = [1, 32, 32, 32]

time_kernel_size = [11, 11, 11]
time_stride = [2, 1, 1]
time_padding = [10, 5, 5]

spec_kernel_size = [35, 20, 16]
spec_stride = [2, 2, 2]
spec_padding = [20, 10, 6]

# Wav model
wav_channels_shapes = [1, 32, 64, 128, 256]
wav_kernel_size = [80, 60, 40, 20]
wav_stride_size = [8, 4, 4, 2]
wav_dilatation_size = [2, 2, 2, 1]
wav_padding_size = [50, 30, 25, 20]
lstm_shape = 256

# validate correct input shapes
for params in [time_kernel_size, time_stride, time_padding,
               spec_kernel_size, spec_stride, spec_padding]:
    assert len(channels_shapes) - 1 == len(params)

train_pred_dataset_types = ['ordered_train', 'test', 'valid'] # train

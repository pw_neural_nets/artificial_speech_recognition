from artifical_speech_recognition.utils import utilities
from artifical_speech_recognition.pipeline import pipeline

utilities.set_random_seed()
training_pipeline = pipeline.ClassificationTraining.from_config()
# training_pipeline = pipeline.ClassificationTraining.from_checkpoint()
training_pipeline.train_model()
preds = training_pipeline.get_predictions(pred_proba=False)
preds.to_csv("train-preds_wav1d2d.csv")

pred_pipeline = pipeline.PredictionPipeline.from_checkpoint(tta=False)
    # load_checkpoint_model_path="./data/model_checkpoints/model.pytorch")
preds = pred_pipeline.get_preds(pred_proba=False)
preds.to_csv("kaggle_submission.csv", index=False)

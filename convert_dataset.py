from artifical_speech_recognition.data_processing import dataset_converter
from artifical_speech_recognition.data_processing import transforms
import config

dc = dataset_converter.SpeechCommandsConverter(
    root=config.data_path,
    folders=["train"],#config.dataset_parameters["MelSpeechCommands"]["folders"],
    save_folder_prefix="mel-notnormed",
    to_spectrogram=transforms.get_spectrogram_transform(),
)

ds = dataset_converter.SpeechCommandsSilenceConverter(
    root=config.data_path,
    folder="train",#config.dataset_parameters["MelSpeechCommands"]["folders"][0],
    save_folder_prefix="mel-notnormed",
    target_suffix=".mel_spec",
    to_spectrogram=transforms.get_spectrogram_transform(),
)

# ds = dataset_converter.SpeechCommandsSilenceConverter(
#     root=config.data_path,
#     folder=config.dataset_parameters["WavSpeechCommands"]["folders"][0],
#     save_folder_prefix="",
#     target_suffix=".wav",
#     to_spectrogram=None,#transforms.get_spectrogram_transform(),
# )


dc.create_spec_dataset()
ds.create_silence_class()

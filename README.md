# Artificial Speech Recognition

## Useful links
[blog with resources](https://heartbeat.fritz.ai/a-2019-guide-for-automatic-speech-recognition-f1e1129a141c)
[book about asr](https://books.google.pl/books?id=8cmcDwAAQBAJ&pg=PA538&lpg=PA538&dq=repeating+characters+asr+output&source=bl&ots=gdZphhVAk9&sig=ACfU3U3VOOznD4R1bbsKZLyhN2cg1wES0A&hl=en&sa=X&ved=2ahUKEwis6NTAh8fpAhUy06YKHY8FCvEQ6AEwCnoECAwQAQ#v=onepage&q=never%20have%20repeated%20characters&f=false)

## CRNN and ResConvLSTM

Data preparation, ResConvLSTM and CRNN are described in Jupyter notebook in notebooks directory.

### TODO

- don't cut samples, just fill shorter samples with silence samples obtained by concatenated silence samples
- optional: get the same amount of samples as silences and create classifier of silence/no-silence to reduce number of classes to 11
- optional: double words - create samples with half word from one and another sample

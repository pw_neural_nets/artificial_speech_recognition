import os
from abc import abstractmethod

import pandas as pd
from sklearn import linear_model
from sklearn import metrics
from sklearn.utils import class_weight
from sklearn import ensemble
from catboost import CatBoostClassifier, Pool
import xgboost as xgb

import config
from artifical_speech_recognition.utils import utilities

# stack ensembles?
# multilayer perceptron / attn on probs
# compare performance when using:
# - train+test for ens train and valid for testing
# - ens on class probabilites
# - ensemble only to improve known classes


class EnsemblePipeline:
    """
    submission_preds_paths and train_predictions_paths should be name in the following format
    <path_to_file>/<mode>_<model_name>.csv
    <mode>: {"submission-preds", "train-preds"}
    """
    def __init__(self, data, submission_data):
        self.data = data
        self.submission_data = submission_data
        self.label_mapping = utilities.get_label_mapping(config.label_mapping_type)

    @abstractmethod
    def predict(self, data):
        pass

    def predict_submission(self):
        y_hat = self.predict(data=self.submission_data)
        y_hat = [utilities.idx_to_label(x) for x in y_hat]
        df = pd.DataFrame(data={"fname": self.submission_data["fname"], "label": y_hat})
        return df

    def score(self):
        for ds_type in pd.unique(self.data["ds_type"]):
            data = self.data[self.data["ds_type"] == ds_type]
            y, y_hat = data["y"].apply(self.label_mapping), self.predict(data)
            score = metrics.accuracy_score(y, y_hat)
            print(f"ds_type: {ds_type}, acc score: {score}")

    @staticmethod
    def get_model_name(path):
        model_name = os.path.basename(path).split(".")[0].split("_")[1]
        return model_name

    @staticmethod
    def load_preds(path, src_column):
        df = pd.read_csv(path)
        model_name = EnsemblePipeline.get_model_name(path)
        if "[" in df[src_column][0]:
            df[src_column] = df[src_column].apply(eval)
            nb_classes = len(df[src_column][0])
            for i in range(nb_classes):
                df[f"y_hat_cls{i}_{model_name}"] = df[src_column].apply(lambda x: x[i])
            df = df.drop(columns=src_column)
            return df
        else:
            df = df.rename(columns={src_column: f"y_hat_{model_name}"})
            return df

    @staticmethod
    def load_data(paths, src_column, merge_key, drop_unk=False, clean_suffix=False):
        path = paths.pop()
        combined_data = EnsemblePipeline.load_preds(path, src_column)
        combined_data = combined_data.sort_values(by=merge_key).reset_index(drop=True)
        if clean_suffix:
            combined_data[merge_key] = combined_data[merge_key].apply(lambda x: x.split(".")[0])
        initial_size = combined_data.shape[0]

        if "y" in combined_data.columns and drop_unk:
            combined_data = combined_data[~(combined_data["y"] == "unknown")]

        for path in paths:
            sub_data = EnsemblePipeline.load_preds(path, src_column)
            if clean_suffix:
                sub_data[merge_key] = sub_data[merge_key].apply(lambda x: x.split(".")[0])

            sub_data = sub_data.sort_values(by=merge_key).reset_index(drop=True)

            # all the same training data assertion
            assert combined_data[merge_key].equals(sub_data[merge_key]), f"{merge_key} is different in models"
            pred_cols = [col for col in sub_data.columns if "y_hat" in col]
            for col in pred_cols:
                combined_data[col] = sub_data[col]
            assert initial_size == combined_data.shape[0], f"Incompatible model predictions in {path}"

            # common part of predicted samples
            # combined_data = pd.merge(sub_data, combined_data, how='inner', on=['fname'], suffixes=("", "_sub"))
            # combined_data = combined_data.drop(columns=[c for c in combined_data if "_sub" in c and "y_hat" not in c])
        return combined_data


class Blender(EnsemblePipeline):
    def __init__(self, data, submission_data, meta_model, train_sets):
        super().__init__(data, submission_data)
        self.meta_model = meta_model
        self.train_sets = train_sets

    def fit(self):
        feature_cols = sorted([col for col in self.data.columns if 'y_hat' in col])
        X = self.data[self.data["ds_type"].isin(self.train_sets)][feature_cols]
        for col in feature_cols:
            X.loc[:, col] = X.loc[:, col].apply(self.label_mapping)
        X = X.values
        y = self.data[self.data["ds_type"].isin(self.train_sets)]["y"].apply(self.label_mapping).values
        self.fit_meta_model(X, y)

    def predict(self, data):
        feature_cols = sorted([col for col in data.columns if 'y_hat' in col])
        X = data[feature_cols]
        for col in feature_cols:
            X.loc[:, col] = X.loc[:, col].apply(self.label_mapping)
        X = X.values
        y_hat = self.predict_meta_model(X)
        return y_hat

    @abstractmethod
    def fit_meta_model(self, X, y):
        pass

    @abstractmethod
    def predict_meta_model(self, X):
        pass

    @classmethod
    @abstractmethod
    def get_default_model(cls):
        pass

    @classmethod
    def from_model(
            cls,
            model=None,
            train_sets=config.ens_train_sets,
            submission_preds_paths=config.submission_preds_paths,
            train_predictions_paths=config.predictions_paths,
    ):
        model = model if model else cls.get_default_model()
        combined_data = cls.load_data(paths=train_predictions_paths, src_column="y_hat", merge_key="fname", drop_unk=False, clean_suffix=True)
        submission_data = cls.load_data(paths=submission_preds_paths, src_column="label", merge_key="fname")
        return cls(meta_model=model, data=combined_data, submission_data=submission_data, train_sets=train_sets)


class SklearnBlender(Blender):
    def fit_meta_model(self, X, y):
        sample_weight = class_weight.compute_sample_weight("balanced", y)
        self.meta_model.fit(X, y, sample_weight)

    def predict_meta_model(self, X):
        return self.meta_model.predict(X)


class WeightedVoter(SklearnBlender):
    # takes for ever and same results linear_model.LogisticRegressionCV(cv=5, random_state=config.SEED, max_iter=1000)
    @classmethod
    def get_default_model(cls):
        return linear_model.LogisticRegression(random_state=config.SEED, max_iter=1000)


class RFBlender(SklearnBlender):
    @classmethod
    def get_default_model(cls):
        return ensemble.RandomForestClassifier(random_state=config.SEED)


class CatBlender(Blender):
    # check out parameter search
    # https://catboost.ai/docs/concepts/python-reference_catboostclassifier_randomized_search.html
    @classmethod
    def get_default_model(cls):
        return CatBoostClassifier(
            iterations=500,
            learning_rate=0.03,
            #depth=10,
            loss_function='MultiClass'
        )

    def fit_meta_model(self, X, y):
        sample_weight = class_weight.compute_sample_weight("balanced", y)
        cat_features = list(range(X.shape[1]))
        X = Pool(
            data=X,
            label=y,
            cat_features=cat_features,
            weight=sample_weight
        )
        self.meta_model.fit(X)

    def predict_meta_model(self, X):
        cat_features = list(range(X.shape[1]))
        X = Pool(
            data=X,
            cat_features=cat_features
        )
        y_hat = self.meta_model.predict(X)
        return [y[0] for y in y_hat]


class XGBlender(Blender):
    @classmethod
    def get_default_model(cls):
        param = {'max_depth': 2, 'eta': 1, 'objective': 'multi:softmax', 'num_round': 2}
        return xgb.XGBClassifier(**param)

    def fit_meta_model(self, X, y):
        data_dmatrix = xgb.DMatrix(data=X, label=y)
        self.meta_model.fit(data_dmatrix) # what is best?

    def predict_meta_model(self, X):
        dtest = xgb.DMatrix(data=X)
        preds = self.meta_model.predict(dtest)
        return preds

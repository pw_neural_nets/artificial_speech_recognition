import itertools

import torch
from torch.utils.tensorboard import SummaryWriter
import numpy as np
from textwrap import wrap
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve, auc
from scipy import interp

import config
from artifical_speech_recognition.utils import utilities

# TODO
# save examples of biggest loss samples


class TensorBoardLogger:
    def __init__(
            self, example_interval, metrics, prediction_mode, one_hot_targets,
            save_metric_name="AccuracyWithoutUnk", save_metric_val=0,
            steps=None, epoch=0, normalize=False,
    ):
        self.writer = SummaryWriter()
        self.mode = "train"
        self.example_interval = example_interval
        self.current_eval_epoch_losses = []
        self.save_metric_val = save_metric_val
        self.save_metric_name = save_metric_name
        self.current_save_metric_val = 0
        self.epoch = epoch
        self.prediction_mode = prediction_mode
        self.metrics = metrics
        self.normalize = normalize
        self.one_hot_targets = one_hot_targets
        self.predictions = self.get_predictions_dict()

        if steps:
            self.steps = steps
        else:
            self.steps = {
                "train": {
                    "step": 0,
                },
                "valid": {
                    "step": 0,
                }
            }

    def __call__(self, loss, preds, targets):
        if len(targets.shape) > 1:
            targets = torch.argmax(targets, dim=-1)

        args = self.steps[self.mode]
        self.predictions[self.mode]["target"].extend(targets.numpy())
        self.predictions[self.mode]["pred_prob"].extend(preds.detach().cpu().numpy())
        if loss is not None:
            self.writer.add_scalar(f'Loss/{self.mode}', loss, args['step'])

        if args['step'] % self.example_interval == 0 and self.prediction_mode != "cls":
            pred_tag = f"pred_ep{self.epoch}_step{args['step']}_mode-{self.mode}"
            target_tag = f"target_ep{self.epoch}_step{args['step']}_mode-{self.mode}"
            self.writer.add_text(tag=pred_tag, text_string=preds[0], global_step=args['step'])
            self.writer.add_text(tag=target_tag, text_string=targets[0], global_step=args['step'])

        if self.mode == "valid":
            self.current_eval_epoch_losses.append(loss)

        args['step'] += 1

    def train(self):
        self.mode = "train"

    def eval(self):
        self.mode = "valid"

    def next_best_epoch(self):
        is_best = False
        for mode, preds in self.predictions.items():
            targets = preds['target']
            predictions = preds["pred_prob"]

            if targets and predictions:
                self.write_metrics(predictions, targets, mode)
                self.write_confusion_matrix(predictions, targets, mode)
            else:
                print("Empty predictions warning")

        if self.save_metric_val < self.current_save_metric_val:
            self.save_metric_val = self.current_save_metric_val
            is_best = True
        self.epoch += 1

        self.predictions = self.get_predictions_dict()

        return is_best

    def __getstate__(self):
        return {
            "epoch": self.epoch,
            "save_metric_val": self.save_metric_val,
            "steps": self.steps,
        }

    def write_metrics(self, predictions, targets, mode):
        for metric in self.metrics:
            metric_score = metric(y_hat=predictions, y=targets)
            metric_name = metric.__class__.__name__
            if metric_name == self.save_metric_name and mode == "valid":
                self.current_save_metric_val = metric_score

            print(f"{metric_name} {metric_score}")
            self.writer.add_scalar(f'{metric_name}/{mode}', metric_score, self.epoch)

    def write_confusion_matrix(self, predictions, targets, mode):
        predictions = np.array(predictions).argmax(axis=1)
        confusion_matrix_fig = self.plot_confusion_matrix(correct_labels=targets, predict_labels=predictions)
        confusion_matrix_tag = f"ConfusionMatrix/{mode}_ep{self.epoch}"
        self.writer.add_figure(tag=confusion_matrix_tag, figure=confusion_matrix_fig, global_step=self.epoch)

    def write_roc_curves(self, predictions, targets, mode):
        targets = np.array(targets)
        one_hot_target = np.zeros((targets.size, len(config.target_labels)))
        one_hot_target[np.arange(targets.size), targets] = 1

        predictions = np.array(predictions)
        # Compute ROC curve and ROC area for each class
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        for i in range(len(config.target_labels)):
            fpr[i], tpr[i], _ = roc_curve(one_hot_target[:, i], predictions[:, i], pos_label=i)
            roc_auc[i] = auc(fpr[i], tpr[i])

        # Compute micro-average ROC curve and ROC area
        fpr["micro"], tpr["micro"], _ = roc_curve(one_hot_target.ravel(), predictions.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

        all_fpr = np.unique(np.concatenate([fpr[i] for i in range(len(config.target_labels))]))

        # Then interpolate all ROC curves at this points
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(len(config.target_labels)):
            mean_tpr += interp(all_fpr, fpr[i], tpr[i])

        # Finally average it and compute AUC
        mean_tpr /= len(config.target_labels)

        fpr["macro"] = all_fpr
        tpr["macro"] = mean_tpr
        roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

        fig = plt.figure(figsize=(7, 7), dpi=320, facecolor='w', edgecolor='k')
        ax = fig.add_subplot(1, 1, 1)

        for i in range(len(config.target_labels)):
            plt.plot(
                fpr[i], tpr[i],
                label='ROC curve of class {0} (area = {1:0.2f})'
                ''.format(i, roc_auc[i])
            )

        plt.plot([0, 1], [0, 1], 'k--')
        ax.set_xlim([0.0, 1.0])
        ax.set_ylim([0.0, 1.05])
        ax.set_xlabel('False Positive Rate')
        ax.set_ylabel('True Positive Rate')
        ax.legend(loc="lower right")

        roc_tag = f"ROC/{mode}_ep{self.epoch}"
        self.writer.add_figure(tag=roc_tag, figure=fig, global_step=self.epoch)

    def get_predictions_dict(self):
        return {mode: {"target": [], "pred_prob": []} for mode in ["train", "valid"]}

    def plot_confusion_matrix(self, correct_labels, predict_labels):
        cm = confusion_matrix(correct_labels, predict_labels, labels=list(range(len(config.target_labels))))
        if self.normalize:
            cm = cm.astype('float')*10 / cm.sum(axis=1)[:, np.newaxis]
            cm = np.nan_to_num(cm, copy=True)
            cm = cm.astype('int')

        np.set_printoptions(precision=2)

        fig = plt.figure(figsize=(7, 7), dpi=320, facecolor='w', edgecolor='k')
        ax = fig.add_subplot(1, 1, 1)
        im = ax.imshow(cm, cmap='Oranges')

        classes = ['\n'.join(wrap(l, 40)) for l in config.target_labels]

        tick_marks = np.arange(len(classes))

        ax.set_xlabel('Predicted', fontsize=7)
        ax.set_xticks(tick_marks)
        c = ax.set_xticklabels(classes, fontsize=4, rotation=-90,  ha='center')
        ax.xaxis.set_label_position('bottom')
        ax.xaxis.tick_bottom()

        ax.set_ylabel('True Label', fontsize=7)
        ax.set_yticks(tick_marks)
        ax.set_yticklabels(classes, fontsize=4, va='center')
        ax.yaxis.set_label_position('left')
        ax.yaxis.tick_left()

        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            ax.text(j, i, format(cm[i, j], 'd') if cm[i, j] != 0
            else '.', horizontalalignment="center", fontsize=6, verticalalignment='center', color="black")
        fig.set_tight_layout(True)
        return fig

    def save_hparams(self, model_name, hidden_lstm):
        self.writer.add_hparams(hparam_dict={
                'dataset': config.dataset_name,
                'model': model_name,
                'lr': config.lr,
                'bs': config.bs,
                'epochs': config.epochs,
                'fc_dropout': config.fc_dropout,
                'channels_shapes': str(config.channels_shapes),
                'bidirectional_lstm': str(config.bidirectional),
                'lstm_size': hidden_lstm,
                'lstm_layers': config.lstm_layers,
                'time_kernel_size': str(config.time_kernel_size),
                'time_stride': str(config.time_stride),
                'time_padding': str(config.time_padding),
                'spec_kernel_size': str(config.spec_kernel_size),
                'spec_stride': str(config.spec_stride),
                'spec_padding': str(config.spec_padding),
            },
            metric_dict={},
        )

    @classmethod
    def from_config(cls, metrics, prediction_mode, one_hot_targets=False):
        return cls(
            metrics=metrics,
            example_interval=config.example_interval,
            prediction_mode=prediction_mode,
            one_hot_targets=one_hot_targets,
        )

    @classmethod
    def from_state(cls, state_dict, metrics, prediction_mode, one_hot_targets=False):
        save_metric_val = state_dict["save_metric_val"]
        steps = state_dict["steps"]
        epoch = state_dict["epoch"]
        return cls(
            metrics=metrics,
            example_interval=config.example_interval,
            save_metric_val=save_metric_val,
            steps=steps,
            epoch=epoch,
            prediction_mode=prediction_mode,
            one_hot_targets=one_hot_targets,
        )

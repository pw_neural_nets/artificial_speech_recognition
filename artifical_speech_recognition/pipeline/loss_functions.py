import torch


class KLDivLossLog:
    def __init__(self):
        self.crit_fn = torch.nn.KLDivLoss()

    def __call__(self, input, target):
        input = torch.log(input)
        x = self.crit_fn(input, target)
        return x

    def to(self, device):
        self.crit_fn.to(device)
        return self

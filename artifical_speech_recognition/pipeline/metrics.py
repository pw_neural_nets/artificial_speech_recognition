import numpy as np
from sklearn.metrics import roc_auc_score, precision_score, recall_score

from artifical_speech_recognition.utils import utilities
from artifical_speech_recognition.data_processing import text_processing
import config


class ClosestLabelAccuracy:
    def __init__(self, special_symbols):
        self.special_symbols = special_symbols

    def __call__(self, y_hat, y):
        predicted_words = y_hat
        target_words = y
        correct_predictions = 0
        sample_size = len(target_words)
        for pred, target in zip(predicted_words, target_words):
            pred = text_processing.remove_repeated_symbols(pred)
            target = text_processing.remove_repeated_symbols(target)
            pred = text_processing.clean_special_symbols(pred)
            target = text_processing.clean_special_symbols(target)
            pred = utilities.get_most_similar_label(pred)

            if pred == target:
                correct_predictions += 1

        accuracy = correct_predictions / sample_size
        return accuracy


class Accuracy:
    def __call__(self, y_hat, y):
        y = np.array(y)
        y_hat = np.array(y_hat).argmax(axis=1)
        return sum(y == y_hat) / float(y.shape[0])


class AccuracyWithoutUnk:
    def __call__(self, y_hat, y):
        y = np.array(y)
        valid_idx = [idx for idx in range(len(y)) if config.target_labels[y[idx]] != "unknown"]
        y = y[valid_idx]
        y_hat = np.array(y_hat).argmax(axis=1)[valid_idx]
        sample_size = len(y) if len(y) else 1
        return np.sum(y == y_hat) / sample_size


class AccuracyWithUnk:
    def __init__(self):
        self.label_mapping = utilities.get_label_to_unk_labels_mapping()

    def __call__(self, y_hat, y):
        y = np.array([self.label_mapping[x.item()] for x in y])
        y_hat = np.array([self.label_mapping[x.argmax(dim=1).item()] for x in y_hat])
        return np.sum(y == y_hat) / float(y.shape[0])


class Recall:
    def __init__(self, average='macro'):
        self.average = average

    def __call__(self, y_hat, y):
        y = np.array(y)
        y_hat = np.array(y_hat).argmax(axis=1)
        return precision_score(y, y_hat, average=self.average)


class Precision:
    def __init__(self, average='macro'):
        self.average = average

    def __call__(self, y_hat, y):
        y = np.array(y)
        y_hat = np.array(y_hat).argmax(axis=1)
        return recall_score(y, y_hat, average=self.average)


class ROC:
    def __init__(self, multi_class="ovr", average="macro"):
        self.multi_class = multi_class
        self.average = average

    def __call__(self, y_hat, y):
        y = np.array(y)
        y_hat = np.array(y_hat)
        try:
            score = roc_auc_score(y, y_hat, multi_class=self.multi_class, average=self.average)
        except Exception as exp:
            print(exp)
            score = np.nan
        return score

from abc import abstractmethod
import os

import torch
from tqdm import tqdm
import pandas as pd

from artifical_speech_recognition.pipeline import loss_functions
from artifical_speech_recognition.data_processing import databunch as db
from artifical_speech_recognition.model.audio import deepspeech
from artifical_speech_recognition.utils import utilities
from artifical_speech_recognition.pipeline import logger as asr_logger
from artifical_speech_recognition.pipeline import metrics
import config

# TODO
# - Pseudo labeling
# - working on amplitude vs db signal - mean


class TrainingPipeline:
    def __init__(self, databunch, model, criterion, optimizer, epochs, logger, device):
        self.model = model
        self.optimizer = optimizer
        self.criterion = criterion
        self.databunch = databunch
        self.logger = logger
        self.epochs = epochs
        self.device = device
        self.TTA_repeat = config.TTA_iterations if config.TTA else 1

    def train_model(self):
        self.logger.save_hparams(model_name=self.model.__class__.__name__, hidden_lstm=self.model.get_lstm_size())
        for epoch in range(self.epochs):
            self.model.train()
            self.logger.train()
            self.run_epoch(phase='train')
            self.model.eval()
            self.logger.eval()
            with torch.no_grad():
                self.run_epoch(phase="valid")
            self.save_checkpoint()

    def run_epoch(self, phase):
        loss = None
        for idx, X, targets in tqdm(self.databunch[phase]):
            X = X.to(self.device)
            y_hat = self.model.forward(X)

            if phase == 'train':
                loss = self.get_loss(targets, y_hat)
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()

            self.log(loss=loss, y=targets, y_hat=y_hat)


    def get_predictions(
            self,
            dataset_types=config.train_pred_dataset_types,
            pred_proba=False,
    ):
        preds_df = pd.DataFrame()
        for ds_type in dataset_types:
            with torch.no_grad():
                for idx, X, targets in tqdm(self.databunch[ds_type]):
                    sub_preds_df = pd.DataFrame()
                    X = X.to(self.device)
                    y_hat_list = torch.stack([self.model.forward(X) for _ in range(self.TTA_repeat)])
                    y_hat = torch.mean(y_hat_list, dim=0)

                    if not pred_proba or config.label_mix:
                        y_hat = y_hat.argmax(dim=1).cpu().numpy()
                        sub_preds_df['y_hat'] = [utilities.idx_to_label(x) for x in y_hat]
                    else:
                        y_hat = y_hat.cpu().numpy()
                        sub_preds_df['y_hat'] = [list(y_hat[i, :]) for i in range(y_hat.shape[0])]

                    sub_preds_df['fname'] = self.get_file_names(
                        idxs=idx.cpu().numpy(),
                        dataset=self.databunch[ds_type].dataset
                    )

                    sub_preds_df['y'] = [utilities.idx_to_label(x) for x in targets.cpu().numpy()]
                    sub_preds_df['ds_type'] = ds_type
                    preds_df = preds_df.append(sub_preds_df)
        return preds_df

    def get_file_names(self, idxs, dataset):
        return [dataset.file_paths[idx][0] + "_" + os.path.basename(dataset.file_paths[idx][1]) for idx in idxs]

    @abstractmethod
    def get_loss(self, targets, y_hat):
        pass

    @abstractmethod
    def log(self, loss, y, y_hat):
        pass

    def save_checkpoint(self):
        if self.logger.next_best_epoch():
            save_dict = {
                "model": self.model.state_dict(),
                "optimizer": self.optimizer.state_dict(),
                "logger_state": self.logger.__getstate__(),
                #"dataset_transformations": self.databunch.get_dataset_transformations(),
                "config": utilities.get_config_dict(),
            }
            torch.save(save_dict, config.save_model_checkpoint_path)


class CTCTraining(TrainingPipeline):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.prediction_mode = "ctc"

    def get_loss(self, targets, y_hat):
        y, y_lengths = targets
        y, y_lengths = y.to(self.device), y_lengths.to(self.device)
        y_hat_lengths = self.get_output_shapes(y_hat)

        loss = self.criterion(
            log_probs=y_hat,
            targets=y,
            input_lengths=y_hat_lengths,
            target_lengths=y_lengths,
        )

        return loss

    def log(self, loss, y, y_hat):
        if config.log:
            y, y_lengths = y
            preds_text, targets_text = self.textify(target=y, pred=y_hat)
            self.logger(
                loss=loss.detach().cpu() if loss is not None else loss,
                preds=preds_text,
                targets=targets_text,
            )

    def textify(self, target, pred):
        preds, targets = [], []
        pred = pred.argmax(dim=2).detach().cpu()
        target = target.cpu()
        for p_idx, t_idx in zip(range(pred.shape[1]), range(target.shape[0])):
            preds.append(self.databunch.textify(pred[:, p_idx])[0])
            targets.append(self.databunch.textify(target[t_idx])[0])
        return preds, targets

    def get_output_shapes(self, y_hat):
        return torch.full(
            size=(y_hat.shape[1],),
            fill_value=y_hat.shape[0],
            dtype=torch.long,
        )

    @classmethod
    def from_config(cls):
        prediction_mode = "ctc"
        databunch = db.TrainingDataBunch.from_config(prediction_mode)
        model = deepspeech.DeepSpeech2.from_config(vocab_size=databunch.get_vocab_size())
        optimizer = torch.optim.Adam(model.parameters(), lr=config.lr)
        criterion = torch.nn.CTCLoss(blank=config.blank_idx)
        metric = [metrics.ClosestLabelAccuracy(special_symbols=config.special_symbols)]
        logger = asr_logger.TensorBoardLogger.from_config(
            metrics=metric,
            prediction_mode=prediction_mode,
        )
        device = utilities.get_device()

        model = model.to(device)
        criterion = criterion.to(device)
        return cls(
            databunch=databunch,
            model=model,
            optimizer=optimizer,
            criterion=criterion,
            epochs=config.epochs,
            logger=logger,
            device=device,
        )

    @classmethod
    def from_checkpoint(cls, load_checkpoint_model_path=config.load_checkpoint_model_path):
        prediction_mode = "ctc"
        device = utilities.get_device()
        checkpoint = torch.load(load_checkpoint_model_path, map_location="cpu")

        # transformations = checkpoint['dataset_transformations']
        databunch = db.TrainingDataBunch.from_parameters(
            #transformations=transformations,
            prediction_mode=prediction_mode,
        )

        model = deepspeech.DeepSpeech2.from_config(vocab_size=databunch.get_vocab_size())
        model.load_state_dict(checkpoint['model'], strict=True)
        model = model.to(device)

        metric = [metrics.ClosestLabelAccuracy(special_symbols=config.special_symbols)]
        logger = asr_logger.TensorBoardLogger.from_state(
            state_dict=checkpoint["logger_state"],
            metrics=metric,
            prediction_mode=prediction_mode,
        )

        optimizer = torch.optim.Adam(model.parameters())
        optimizer.load_state_dict(checkpoint['optimizer'])

        criterion = torch.nn.CTCLoss(blank=config.blank_idx)
        criterion = criterion.to(device)

        return cls(
            databunch=databunch,
            model=model,
            optimizer=optimizer,
            criterion=criterion,
            epochs=config.epochs,
            logger=logger,
            device=device,
        )


class ClassificationTraining(TrainingPipeline):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.prediction_mode = "cls"

    @staticmethod
    def get_criterion(label_mix):
        # weights = databunch.get_class_weights().to(device)
        # weighting is done via sampling probabilities
        if label_mix:
            criterion = loss_functions.KLDivLossLog()
        else:
            criterion = torch.nn.CrossEntropyLoss() # weight=weights)
        return criterion

    @staticmethod
    def get_model():
        suffix = config.dataset_parameters[config.dataset_name]['suffix']
        if suffix == ".mel_spec":
            model = deepspeech.AttentionClassifier.from_config(label_mapping=config.label_mapping)
        elif suffix == ".wav":
            if config.wav_model == "1d2d":
                model = deepspeech.WavDIYSpecClassifier.from_config(label_mapping=config.label_mapping)
            else:
                model = deepspeech.WavClassifier.from_config(label_mapping=config.label_mapping)
        else:
            raise ValueError(f"No models support suffix {suffix}")
        return model

    @classmethod
    def from_config(cls):
        prediction_mode = "cls"
        device = utilities.get_device()
        databunch = db.TrainingDataBunch.from_config(prediction_mode)
        model = cls.get_model()
        optimizer = torch.optim.Adam(model.parameters(), lr=config.lr)
        criterion = cls.get_criterion(config.label_mix)
        exp_metric = [metrics.Accuracy(), metrics.AccuracyWithoutUnk(), metrics.Precision(), metrics.Recall()]
        logger = asr_logger.TensorBoardLogger.from_config(
            metrics=exp_metric,
            prediction_mode=prediction_mode,
            one_hot_targets=config.label_mix,
        )

        model = model.to(device)
        criterion = criterion.to(device)
        return cls(
            databunch=databunch,
            model=model,
            optimizer=optimizer,
            criterion=criterion,
            epochs=config.epochs,
            logger=logger,
            device=device,
        )

    @classmethod
    def from_checkpoint(cls, load_checkpoint_model_path=config.load_checkpoint_model_path):
        prediction_mode = "cls"
        device = utilities.get_device()
        checkpoint = torch.load(load_checkpoint_model_path, map_location="cpu")

        # transformations = checkpoint['dataset_transformations']
        databunch = db.TrainingDataBunch.from_parameters(
            #transformations=transformations,
            prediction_mode=prediction_mode
        )

        model = cls.get_model()
        model.load_state_dict(checkpoint['model'], strict=True)
        model = model.to(device)

        metric = [metrics.Accuracy(), metrics.AccuracyWithoutUnk(), metrics.Precision(), metrics.Recall()]
        logger = asr_logger.TensorBoardLogger.from_state(
            state_dict=checkpoint["logger_state"],
            metrics=metric,
            prediction_mode=prediction_mode,
            one_hot_targets=config.label_mix,
        )

        optimizer = torch.optim.Adam(model.parameters())
        optimizer.load_state_dict(checkpoint['optimizer'])

        criterion = cls.get_criterion(config.label_mix)
        criterion = criterion.to(device)

        return cls(
            databunch=databunch,
            model=model,
            optimizer=optimizer,
            criterion=criterion,
            epochs=config.epochs,
            logger=logger,
            device=device,
        )

    def get_loss(self, y, y_hat):
        y = y.to(self.device)
        loss = self.criterion(
            input=y_hat,
            target=y,
        )
        return loss

    def log(self, loss, y, y_hat):
        if config.log:
            y_hat = y_hat.cpu()
            self.logger(
                loss=loss.detach().cpu() if loss is not None else loss,
                preds=y_hat,
                targets=y,
            )


class PredictionPipeline:
    def __init__(self, model, databunch, device):
        self.databunch = databunch
        self.model = model
        self.model.eval()
        self.device = device
        self.label_mapping = utilities.get_label_to_unk_labels_mapping()

    def get_preds(self, pred_proba):
        preds_df = pd.DataFrame()
        for samples in tqdm(zip(*self.databunch.datasets)):
            sub_preds_df = pd.DataFrame()
            tta_preds = []
            for idx, X in samples:
                X = X.to(self.device)
                with torch.no_grad():
                    y_hat = self.model.forward(X)
                tta_preds.append(y_hat)

            tta_preds = torch.stack(tta_preds)
            y_hat = torch.mean(tta_preds, dim=0)
            sub_preds_df['fname'] = [os.path.basename(self.databunch.file_names[i][1]) for i in idx.tolist()]

            if pred_proba:
                y_hat = y_hat.cpu().numpy()
                sub_preds_df['label'] = [list(y_hat[i, :]) for i in range(y_hat.shape[0])]
            else:
                y_hat = y_hat.argmax(dim=1).cpu().numpy()
                sub_preds_df['label'] = [utilities.idx_to_label(x) for x in y_hat]
            preds_df = preds_df.append(sub_preds_df)
        return preds_df

    @staticmethod
    def get_model():
        suffix = config.dataset_parameters[config.dataset_name]['suffix']
        if suffix == ".mel_spec":
            model = deepspeech.AttentionClassifier.from_config(label_mapping=config.label_mapping)
        elif suffix == ".wav":
            if config.wav_model == "1d2d":
                model = deepspeech.WavDIYSpecClassifier.from_config(label_mapping=config.label_mapping)
            else:
                model = deepspeech.WavClassifier.from_config(label_mapping=config.label_mapping)
        else:
            raise ValueError(f"No models support suffix {suffix}")
        return model

    @classmethod
    def from_checkpoint(cls, tta=config.TTA, load_checkpoint_model_path=config.load_checkpoint_model_path):
        device = utilities.get_device()
        suffix = config.dataset_parameters[config.dataset_name]['suffix']
        checkpoint = torch.load(load_checkpoint_model_path, map_location="cpu")

        # transformations = checkpoint['dataset_transformations'] if tta else None
        test_time_aug_iterations = config.TTA_iterations if tta else 1
        databunch = db.PredictionDataBunch.from_config(
            # transformations=transformations,
            suffix=suffix,
            tta_iter=test_time_aug_iterations
        )
        model = cls.get_model()
        model.load_state_dict(checkpoint['model'], strict=True)
        model = model.to(device)
        return cls(
            databunch=databunch,
            model=model,
            device=device,
        )

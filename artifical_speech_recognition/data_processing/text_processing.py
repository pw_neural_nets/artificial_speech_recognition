import collections
import string
import re

import config


class CharacterVocab:
    def __init__(self, itos, special_symbols):
        self.itos = itos
        self.stoi = collections.defaultdict(int, {v: k for k, v in enumerate(self.itos)})
        self.special_symbols = special_symbols

    def get_size(self):
        return len(self.itos)

    def numericalize(self, t):
        return [self.stoi[c] for c in t]

    def textify(self, nums, sep='', clean=False):
        text = sep.join([self.itos[i] for i in nums])
        if clean:
            text = text.replace(self.itos[config.blank_idx], "")
        return text

    def __call__(self, token):
        return self.numericalize(token)

    @staticmethod
    def get_ascii_characters():
        #digits = list('0123456789')
        #punctuation = list('!"#$%&\'()*+,-./:;<=>?@[\\]_`{|}~')
        whitespace = list(' ')#\t\n
        #characters = list(string.ascii_letters)
        characters = list(string.ascii_lowercase)
        #characters.extend(digits)
        #characters.extend(punctuation)
        characters.extend(whitespace)
        return characters

    @classmethod
    def from_ascii(cls):
        symbols_mapping = cls.get_ascii_characters()
        for sc in reversed(config.special_symbols):
            if sc in symbols_mapping:
                symbols_mapping.remove(sc)
            symbols_mapping.insert(0, sc)
        return cls(itos=symbols_mapping, special_symbols=config.special_symbols)


def remove_repeated_symbols(string):
    pattern = re.compile(r"(\w)\1*")
    substitution = r'\1'
    return pattern.sub(substitution, string)


def clean_special_symbols(string):
    for special_symbol in config.special_symbols:
        string = string.replace(special_symbol, "")
    return string

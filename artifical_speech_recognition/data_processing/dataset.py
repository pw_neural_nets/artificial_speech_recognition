from abc import abstractmethod

import torch
from torch.utils import data as tdataset

import config
from artifical_speech_recognition.utils import utilities
from artifical_speech_recognition.data_processing import transforms

class ASRDataset(tdataset.Dataset):
    def __init__(self, augmentation):
        self.augmentation = augmentation

    @abstractmethod
    def __len__(self):
        pass

    @abstractmethod
    def get_data(self, index):
        pass


class TextASRDataset(ASRDataset):
    def __init__(self, vocab, augmentation):
        super().__init__(augmentation)
        self.vocab = vocab

    def __getitem__(self, index):
        sample = self.get_data(index)
        if self.augmentation:
            sample = self.augmentation(sample)
        return index, sample

    def get_vocab_size(self):
        return self.vocab.get_size()

    def numericalize(self, t):
        return self.vocab.numericalize(t)

    def textify(self, nums):
        return self.vocab.textify(nums)


class ClassificationDataset(ASRDataset):
    def __init__(self, augmentation, label_mapping):
        super().__init__(augmentation)
        self.label_mapping = label_mapping

    def __getitem__(self, index):
        sample, label = self.get_data(index)
        if self.augmentation:
            sample = self.augmentation(sample)
        label = self.label_mapping(label)
        label = torch.LongTensor([label])
        return index, sample, label


class ClassificationLabelMixDataset(ASRDataset):
    # augmenting silence samples seems like a bad idea
    def __init__(self, augmentation, label_mapping, mix_probability,
                 class_count, max_audio_length, sampler):
        super().__init__(augmentation)
        self.mix_probability = mix_probability
        self.label_mapping = label_mapping
        self.class_count = class_count
        self.max_audio_length = max_audio_length
        self.sampler = iter(sampler)

    def __getitem__(self, index):
        sample, label = self.get_data(index)
        mixed_sample, mixed_label = self.get_mixed_samples(sample, label)

        if self.augmentation:
            mixed_sample = self.augmentation(mixed_sample)
        return index, mixed_sample, mixed_label

    def get_mixed_samples(self, sample, label):
        one_hot_label = self.get_label_vector(label)
        sample = self.pad_audio(sample)
        if self.mix_probability > torch.rand((1,)) or label == "silence":
            mixed_sample = sample
            mixed_label = one_hot_label
        else:
            rdm_sample, rdm_label = self.get_random_sample()
            proportion = torch.rand((1,)) * 0.40
            mixed_sample = sample * proportion + rdm_sample * (1 - proportion)
            mixed_label = one_hot_label * proportion + rdm_label * (1 - proportion)
        return mixed_sample, mixed_label

    def get_random_sample(self):
        # rdm_idx = torch.randint(0, len(self), (1,))
        rdm_idx = next(self.sampler)
        sample, label = self.get_data(rdm_idx)
        if label == "silence":
            return self.get_random_sample()
        else:
            label = self.get_label_vector(label)
            sample = self.pad_audio(sample)
            return sample, label

    def get_label_vector(self, label):
        label = self.label_mapping(label)
        class_probabilities = torch.zeros((self.class_count,))
        class_probabilities[label] = 1
        return class_probabilities

    def pad_audio(self, sample):
        if sample.shape[-1] != self.max_audio_length:
            max_len_sample = torch.zeros((*sample.shape[:-1], self.max_audio_length))
            max_len_sample[:, :, :sample.shape[-1]] = sample
        else:
            max_len_sample = sample
        return max_len_sample


class AudioDataset(TextASRDataset):
    """
    convert wav to spectrograms
    convert text to labels
    support augmentation
    """
    def __init__(self, wav_paths, vocab, augmentation, to_spectrogram):
        super().__init__(vocab, augmentation)
        self.to_spectrogram = to_spectrogram
        self.wav_paths = wav_paths

    def get_data(self, index):
        sample = self.load_sample(index)
        if self.to_spectrogram:
            sample = self.to_spectrogram(sample)
        return sample

    def load_sample(self, index):
        _, path = self.wav_paths[index]
        waveform, sample_rate = utilities.load_audio(path)
        return waveform

    def __len__(self):
        return len(self.wav_paths)


class MelSpectrogramDataset:
    def __init__(self, file_paths):
        self.file_paths = file_paths

    def __len__(self):
        return len(self.file_paths)

    def get_data(self, index):
        label, path = self.file_paths[index]
        spectrogram = torch.load(path)
        spectrogram = spectrogram if len(spectrogram.shape) == 3 else spectrogram.unsqueeze(0)
        return spectrogram, label


class WavDataset:
    def __init__(self, file_paths):
        self.file_paths = file_paths

    def __len__(self):
        return len(self.file_paths)

    def get_data(self, index):
        label, path = self.file_paths[index]
        waveform, sample_rate = utilities.load_audio(path)
        return waveform, label


class MelClassification(MelSpectrogramDataset, ClassificationDataset):
    def __init__(self, file_paths, augmentation, label_mapping, **kwargs):
        MelSpectrogramDataset.__init__(self, file_paths=file_paths)
        ClassificationDataset.__init__(self, augmentation=augmentation, label_mapping=label_mapping)


class MelCTCDataset(MelSpectrogramDataset, TextASRDataset):
    def __init__(self, file_paths, vocab, augmentation, **kwargs):
        TextASRDataset.__init__(self, augmentation=augmentation, vocab=vocab)
        MelSpectrogramDataset.__init__(self, file_paths=file_paths)


class WavClassification(WavDataset, ClassificationDataset):
    def __init__(self, file_paths, augmentation, label_mapping, **kwargs):
        WavDataset.__init__(self, file_paths=file_paths)
        ClassificationDataset.__init__(self, augmentation=augmentation, label_mapping=label_mapping)


class MelLabelMix(MelSpectrogramDataset, ClassificationLabelMixDataset):
    def __init__(self, file_paths, augmentation, label_mapping, sampler,
                 mix_probability=config.label_mix_prob,
                 class_count=len(config.target_labels), **kwargs):
        MelSpectrogramDataset.__init__(self, file_paths=file_paths)
        ClassificationLabelMixDataset.__init__(
            self,
            augmentation=augmentation,
            label_mapping=label_mapping,
            mix_probability=mix_probability,
            class_count=class_count,
            max_audio_length=config.max_mel_length,
            sampler=sampler,
        )


class WavLabelMix(WavDataset, ClassificationLabelMixDataset):
    def __init__(self, file_paths, augmentation, label_mapping, sampler,
                 mix_probability=config.label_mix_prob,
                 class_count=len(config.target_labels), **kwargs):
        WavDataset.__init__(self, file_paths=file_paths)
        ClassificationLabelMixDataset.__init__(
            self,
            augmentation=augmentation,
            label_mapping=label_mapping,
            mix_probability=mix_probability,
            class_count=class_count,
            max_audio_length=config.max_wav_length,
            sampler=sampler,
        )


def get_dataset(prediction_mode, suffix, label_mix):
    if prediction_mode == "cls" and suffix == ".mel_spec":
        if label_mix:
            return MelLabelMix
        else:
            return MelClassification
    elif prediction_mode == "cls" and suffix == ".wav":
        if label_mix:
            return WavLabelMix
        else:
            return WavClassification
    elif prediction_mode == "ctc":
        return MelCTCDataset
    else:
        raise ValueError(f"Prediction mode {prediction_mode} and suffix {suffix} combination is not supported")

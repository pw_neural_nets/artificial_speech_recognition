import random

import numpy as np
import torch
from torch.nn.utils.rnn import pad_sequence
import torchaudio
import torch.nn.functional as F
import librosa

import config
from artifical_speech_recognition.utils import utilities

"""
Augmentation
SpecAugment: https://arxiv.org/abs/1904.08779v2
Implementation: https://github.com/DemisEom/SpecAugment
mel-spec should be normalized to mean 0 when using this augmentation

For mel spec
0. clear signal + noise
1. warping
https://github.com/zcaceres/spec_augment/blob/77335b74efc453c6bcd589357e8365ddcb7688aa/exp/nb_SparseImageWarp.py
2. time masking
3. freq masking

Auf for waveform from torch audio
1. TimeStretch
2. Fade
3. Vol
"""


class Compose(object):
    def __init__(self, transforms):
        self.transforms = transforms

    def __call__(self, x):
        for t in self.transforms:
            x = t(x)
        return x

    def __repr__(self):
        format_string = self.__class__.__name__ + '('
        for t in self.transforms:
            format_string += '\n'
            format_string += '    {0}'.format(t)
        format_string += '\n)'
        return format_string


class ToTensor(object):
    def __call__(self, x):
        return torch.tensor(x).to(dtype=torch.float32)

    def __repr__(self):
        return self.__class__.__name__ + '()'


def get_spectrogram_transform():
    def to_spectrogram(signal):
        if isinstance(signal, torch.Tensor):
            signal = signal.squeeze().numpy()

        spec = librosa.stft(
            signal,
            n_fft=config.n_fft,
            hop_length=config.hop_length,
            center=False
        )

        dec_spec = librosa.amplitude_to_db(spec, ref=np.max)
        mel_spec = librosa.feature.melspectrogram(S=dec_spec, n_mels=config.n_mels).T
        return torch.from_numpy(mel_spec.astype(np.float32)).unsqueeze(dim=0).permute([0, 2, 1])
    return to_spectrogram


def get_resample_transform(
        data_sample_rate=config.dataset_parameters[config.dataset_name]['sample_rate']
):
    return torchaudio.transforms.Resample(
        data_sample_rate,
        config.sample_rate,
    )


class ToDecibel:
    def __init__(self, mean):
        self.mean = mean

    def __call__(self, sample):
        sample = librosa.amplitude_to_db(sample.numpy(), ref=np.max)
        sample = sample - self.mean
        return torch.from_numpy(sample)


class WavEdgeZeroing:
    """
    Set to 0 random part of beginning and end of waveform signal
    """
    def __init__(self, proportion, transformation_prob):
        self.proportion = proportion
        self.transformation_prob = transformation_prob

    def __call__(self, signal):
        if torch.rand((1,)) > self.transformation_prob:
            return signal

        signal_length = signal.shape[-1]
        front = int(torch.rand((1,)) * self.proportion * signal_length)
        back = int((1 - self.proportion * torch.rand((1,))) * signal_length)
        signal[:, 0:front] = 0
        signal[:, back:signal_length] = 0
        return signal


class WavNoiseInject:
    def __init__(self, noise_samples, max_noise_level, transformation_prob):
        self.noise_samples = noise_samples
        self.max_noise_level = max_noise_level
        self.transformation_prob = transformation_prob

    def __call__(self, signal):
        if torch.rand((1,)) > self.transformation_prob:
            return signal

        noise = self.get_rdm_noise()
        weight = self.get_loudness()

        trimmed_noise = self.trimm_noise_length(signal, noise)
        noised_mel_spec = (1 - weight) * signal + weight * trimmed_noise
        return noised_mel_spec

    def trimm_noise_length(self, signal, noise):
        signal_length = signal.shape[-1]
        noise_length = noise.shape[-1]

        max_noise_idx = torch.randint(signal_length, noise_length, (1,))
        trimmed_noise = noise[:, max_noise_idx-signal_length:max_noise_idx]
        return trimmed_noise

    def get_rdm_noise(self):
        sample_nb = torch.randint(0, len(self.noise_samples) - 1, (1,))
        return self.noise_samples[sample_nb]

    def get_loudness(self):
        return torch.rand([1]) * self.max_noise_level

    @staticmethod
    def get_noise_combinations(noise, num_splits):
        noise_samples = torch.split(noise, int(noise.shape[-1] / num_splits), dim=1)[:-1]
        return noise_samples

    @classmethod
    def from_file(cls, noise_file_paths):
        noise_samples = []
        for single_noise_path in noise_file_paths:
            noise, _ = utilities.load_audio(single_noise_path)
            samples = cls.get_noise_combinations(
                noise=noise,
                num_splits=config.noise_signal_splits,
            )
            noise_samples.extend(samples)

        return cls(
            noise_samples=noise_samples,
            max_noise_level=config.max_noise_level,
            transformation_prob=config.augmentation_probability,
        )


class MinMaxScale:
    def __init__(self, mean):
        self.mean = mean

    def __call__(self, sample):
        return sample - self.mean


class MaskSpectrogramFreq:
    def __init__(self, max_mask_len):
        self.max_mask_len = max_mask_len

    def __call__(self, mel_spectrogram):
        signal_length = mel_spectrogram.shape[1]
        start_idx, end_idx = self.get_random_mask(signal_length)
        mel_spectrogram[:, start_idx:end_idx, :] = torch.zeros_like(mel_spectrogram)[:, start_idx:end_idx, :]
        return mel_spectrogram

    def get_random_mask(self, signal_length):
        freq_mask_len = torch.randint(0, self.max_mask_len, (1,))
        end_idx = torch.randint(freq_mask_len.item(), signal_length, (1,))
        start_idx = end_idx - freq_mask_len
        return start_idx, end_idx


class MaskSpectrogramTime:
    def __init__(self, max_mask_len):
        self.max_mask_len = max_mask_len

    def __call__(self, mel_spectrogram):
        signal_length = mel_spectrogram.shape[2]
        start_idx, end_idx = self.get_random_mask(signal_length)
        mel_spectrogram[:, :, start_idx:end_idx] = torch.zeros_like(mel_spectrogram)[:, :, start_idx:end_idx]
        return mel_spectrogram

    def get_random_mask(self, signal_length):
        time_mask_len = torch.randint(0, self.max_mask_len, (1,))
        end_idx = torch.randint(time_mask_len.item(), signal_length, (1,))
        start_idx = end_idx - time_mask_len
        return start_idx, end_idx


class WarpTime:
    """
    source: https://github.com/DemisEom/SpecAugment
    """
    def __init__(self, speed):
        self.speed = speed

    def __call__(self, spec):
        num_rows = spec.shape[1]
        spec_len = spec.shape[2]

        y = num_rows // 2
        horizontal_line_at_ctr = spec[0][y]
        # assert len(horizontal_line_at_ctr) == spec_len

        point_to_warp = horizontal_line_at_ctr[random.randrange(self.speed, spec_len-self.speed)]
        # assert isinstance(point_to_warp, torch.Tensor)

        # Uniform distribution from (0,W) with chance to be up to W negative
        dist_to_warp = random.randrange(-self.speed, self.speed)
        src_pts = torch.tensor([[[y, point_to_warp]]])
        dest_pts = torch.tensor([[[y, point_to_warp + dist_to_warp]]])
        warped_spectro, dense_flows = self.sparse_image_warp(spec, src_pts, dest_pts)

        return warped_spectro.squeeze(3)

    def sparse_image_warp(
            self,
            img_tensor,
            source_control_point_locations,
            dest_control_point_locations,
            interpolation_order=2,
            regularization_weight=0.0,
            num_boundaries_points=0
    ):

        control_point_flows = (dest_control_point_locations - source_control_point_locations)

        batch_size, image_height, image_width = img_tensor.shape
        grid_locations = self.get_grid_locations(image_height, image_width)
        flattened_grid_locations = torch.tensor(self.flatten_grid_locations(grid_locations, image_height, image_width))

        flattened_flows = self.interpolate_spline(
            dest_control_point_locations,
            control_point_flows,
            flattened_grid_locations,
            interpolation_order,
            regularization_weight)

        dense_flows = self.create_dense_flows(flattened_flows, batch_size, image_height, image_width)

        warped_image = self.dense_image_warp(img_tensor, dense_flows)

        return warped_image, dense_flows


    def get_grid_locations(self, image_height, image_width):
        """Wrapper for np.meshgrid."""

        y_range = np.linspace(0, image_height - 1, image_height)
        x_range = np.linspace(0, image_width - 1, image_width)
        y_grid, x_grid = np.meshgrid(y_range, x_range, indexing='ij')
        return np.stack((y_grid, x_grid), -1)


    def flatten_grid_locations(self, grid_locations, image_height, image_width):
        return np.reshape(grid_locations, [image_height * image_width, 2])


    def create_dense_flows(self, flattened_flows, batch_size, image_height, image_width):
        # possibly .view
        return torch.reshape(flattened_flows, [batch_size, image_height, image_width, 2])


    def interpolate_spline(self, train_points, train_values, query_points, order, regularization_weight=0.0, ):
        # First, fit the spline to the observed data.
        w, v = self.solve_interpolation(train_points, train_values, order, regularization_weight)
        # Then, evaluate the spline at the query locations.
        query_values = self.apply_interpolation(query_points, train_points, w, v, order)

        return query_values


    def solve_interpolation(self, train_points, train_values, order, regularization_weight):
        b, n, d = train_points.shape
        k = train_values.shape[-1]

        # First, rename variables so that the notation (c, f, w, v, A, B, etc.)
        # follows https://en.wikipedia.org/wiki/Polyharmonic_spline.
        # To account for python style guidelines we use
        # matrix_a for A and matrix_b for B.

        c = train_points
        f = train_values.float()

        matrix_a = self.phi(self.cross_squared_distance_matrix(c, c), order).unsqueeze(0)  # [b, n, n]
        #     if regularization_weight > 0:
        #         batch_identity_matrix = array_ops.expand_dims(
        #           linalg_ops.eye(n, dtype=c.dtype), 0)
        #         matrix_a += regularization_weight * batch_identity_matrix

        # Append ones to the feature values for the bias term in the linear model.
        ones = torch.ones(1, dtype=train_points.dtype).view([-1, 1, 1])
        matrix_b = torch.cat((c, ones), 2).float()  # [b, n, d + 1]

        # [b, n + d + 1, n]
        left_block = torch.cat((matrix_a, torch.transpose(matrix_b, 2, 1)), 1)

        num_b_cols = matrix_b.shape[2]  # d + 1

        # In Tensorflow, zeros are used here. Pytorch gesv fails with zeros for some reason we don't understand.
        # So instead we use very tiny randn values (variance of one, zero mean) on one side of our multiplication.
        lhs_zeros = torch.randn((b, num_b_cols, num_b_cols)) / 1e10
        right_block = torch.cat((matrix_b, lhs_zeros),
                                1)  # [b, n + d + 1, d + 1]
        lhs = torch.cat((left_block, right_block),
                        2)  # [b, n + d + 1, n + d + 1]

        rhs_zeros = torch.zeros((b, d + 1, k), dtype=train_points.dtype).float()
        rhs = torch.cat((f, rhs_zeros), 1)  # [b, n + d + 1, k]

        # Then, solve the linear system and unpack the results.
        X, LU = torch.solve(rhs, lhs)
        w = X[:, :n, :]
        v = X[:, n:, :]

        return w, v

    def cross_squared_distance_matrix(self, x, y):
        """Pairwise squared distance between two (batch) matrices' rows (2nd dim).
            Computes the pairwise distances between rows of x and rows of y
            Args:
            x: [batch_size, n, d] float `Tensor`
            y: [batch_size, m, d] float `Tensor`
            Returns:
            squared_dists: [batch_size, n, m] float `Tensor`, where
            squared_dists[b,i,j] = ||x[b,i,:] - y[b,j,:]||^2
        """
        x_norm_squared = torch.sum(torch.mul(x, x))
        y_norm_squared = torch.sum(torch.mul(y, y))

        x_y_transpose = torch.matmul(x.squeeze(0), y.squeeze(0).transpose(0, 1))

        # squared_dists[b,i,j] = ||x_bi - y_bj||^2 = x_bi'x_bi- 2x_bi'x_bj + x_bj'x_bj
        squared_dists = x_norm_squared - 2 * x_y_transpose + y_norm_squared

        return squared_dists.float()


    def phi(self, r, order):
        """Coordinate-wise nonlinearity used to define the order of the interpolation.
        See https://en.wikipedia.org/wiki/Polyharmonic_spline for the definition.
        Args:
        r: input op
        order: interpolation order
        Returns:
        phi_k evaluated coordinate-wise on r, for k = r
        """
        EPSILON = torch.tensor(1e-10)
        # using EPSILON prevents log(0), sqrt0), etc.
        # sqrt(0) is well-defined, but its gradient is not
        if order == 1:
            r = torch.max(r, EPSILON)
            r = torch.sqrt(r)
            return r
        elif order == 2:
            return 0.5 * r * torch.log(torch.max(r, EPSILON))
        elif order == 4:
            return 0.5 * torch.square(r) * torch.log(torch.max(r, EPSILON))
        elif order % 2 == 0:
            r = torch.max(r, EPSILON)
            return 0.5 * torch.pow(r, 0.5 * order) * torch.log(r)
        else:
            r = torch.max(r, EPSILON)
            return torch.pow(r, 0.5 * order)

    def apply_interpolation(self, query_points, train_points, w, v, order):
        """Apply polyharmonic interpolation model to data.
        Given coefficients w and v for the interpolation model, we evaluate
        interpolated function values at query_points.
        Args:
        query_points: `[b, m, d]` x values to evaluate the interpolation at
        train_points: `[b, n, d]` x values that act as the interpolation centers
                        ( the c variables in the wikipedia article)
        w: `[b, n, k]` weights on each interpolation center
        v: `[b, d, k]` weights on each input dimension
        order: order of the interpolation
        Returns:
        Polyharmonic interpolation evaluated at points defined in query_points.
        """
        query_points = query_points.unsqueeze(0)
        # First, compute the contribution from the rbf term.
        pairwise_dists = self.cross_squared_distance_matrix(query_points.float(), train_points.float())
        phi_pairwise_dists = self.phi(pairwise_dists, order)

        rbf_term = torch.matmul(phi_pairwise_dists, w)

        # Then, compute the contribution from the linear term.
        # Pad query_points with ones, for the bias term in the linear model.
        ones = torch.ones_like(query_points[..., :1])
        query_points_pad = torch.cat((
            query_points,
            ones
        ), 2).float()
        linear_term = torch.matmul(query_points_pad, v)

        return rbf_term + linear_term


    def dense_image_warp(self, image, flow):
        """Image warping using per-pixel flow vectors.
        Apply a non-linear warp to the image, where the warp is specified by a dense
        flow field of offset vectors that define the correspondences of pixel values
        in the output image back to locations in the  source image. Specifically, the
        pixel value at output[b, j, i, c] is
        images[b, j - flow[b, j, i, 0], i - flow[b, j, i, 1], c].
        The locations specified by this formula do not necessarily map to an int
        index. Therefore, the pixel value is obtained by bilinear
        interpolation of the 4 nearest pixels around
        (b, j - flow[b, j, i, 0], i - flow[b, j, i, 1]). For locations outside
        of the image, we use the nearest pixel values at the image boundary.
        Args:
        image: 4-D float `Tensor` with shape `[batch, height, width, channels]`.
        flow: A 4-D float `Tensor` with shape `[batch, height, width, 2]`.
        name: A name for the operation (optional).
        Note that image and flow can be of type tf.half, tf.float32, or tf.float64,
        and do not necessarily have to be the same type.
        Returns:
        A 4-D float `Tensor` with shape`[batch, height, width, channels]`
        and same type as input image.
        Raises:
        ValueError: if height < 2 or width < 2 or the inputs have the wrong number
        of dimensions.
        """
        image = image.unsqueeze(3)  # add a single channel dimension to image tensor
        batch_size, height, width, channels = image.shape

        # The flow is defined on the image grid. Turn the flow into a list of query
        # points in the grid space.
        grid_x, grid_y = torch.meshgrid(
            torch.arange(width), torch.arange(height))

        stacked_grid = torch.stack((grid_y, grid_x), dim=2).float()

        batched_grid = stacked_grid.unsqueeze(-1).permute(3, 1, 0, 2)

        query_points_on_grid = batched_grid - flow
        query_points_flattened = torch.reshape(query_points_on_grid,
                                               [batch_size, height * width, 2])
        # Compute values at the query points, then reshape the result back to the
        # image grid.
        interpolated = self.interpolate_bilinear(image, query_points_flattened)
        interpolated = torch.reshape(interpolated,
                                     [batch_size, height, width, channels])
        return interpolated


    def interpolate_bilinear(
            self,
            grid,
            query_points,
            name='interpolate_bilinear',
            indexing='ij'
    ):
        """Similar to Matlab's interp2 function.
        Finds values for query points on a grid using bilinear interpolation.
        Args:
        grid: a 4-D float `Tensor` of shape `[batch, height, width, channels]`.
        query_points: a 3-D float `Tensor` of N points with shape `[batch, N, 2]`.
        name: a name for the operation (optional).
        indexing: whether the query points are specified as row and column (ij),
          or Cartesian coordinates (xy).
        Returns:
        values: a 3-D `Tensor` with shape `[batch, N, channels]`
        Raises:
        ValueError: if the indexing mode is invalid, or if the shape of the inputs
          invalid.
        """
        if indexing != 'ij' and indexing != 'xy':
            raise ValueError('Indexing mode must be \'ij\' or \'xy\'')

        shape = grid.shape
        if len(shape) != 4:
            msg = 'Grid must be 4 dimensional. Received size: '
            raise ValueError(msg + str(grid.shape))

        batch_size, height, width, channels = grid.shape

        shape = [batch_size, height, width, channels]
        query_type = query_points.dtype
        grid_type = grid.dtype

        num_queries = query_points.shape[1]

        alphas = []
        floors = []
        ceils = []
        index_order = [0, 1] if indexing == 'ij' else [1, 0]
        unstacked_query_points = query_points.unbind(2)

        for dim in index_order:
            queries = unstacked_query_points[dim]

            size_in_indexing_dimension = shape[dim + 1]

            # max_floor is size_in_indexing_dimension - 2 so that max_floor + 1
            # is still a valid index into the grid.
            max_floor = torch.tensor(size_in_indexing_dimension - 2, dtype=query_type)
            min_floor = torch.tensor(0.0, dtype=query_type)
            maxx = torch.max(min_floor, torch.floor(queries))
            floor = torch.min(maxx, max_floor)
            int_floor = floor.long()
            floors.append(int_floor)
            ceil = int_floor + 1
            ceils.append(ceil)

            # alpha has the same type as the grid, as we will directly use alpha
            # when taking linear combinations of pixel values from the image.
            alpha = queries.clone().detach() - floor.clone().detach()
            min_alpha = torch.tensor(0.0, dtype=grid_type)
            max_alpha = torch.tensor(1.0, dtype=grid_type)
            alpha = torch.min(torch.max(min_alpha, alpha), max_alpha)

            # Expand alpha to [b, n, 1] so we can use broadcasting
            # (since the alpha values don't depend on the channel).
            alpha = torch.unsqueeze(alpha, 2)
            alphas.append(alpha)

        flattened_grid = torch.reshape(
            grid, [batch_size * height * width, channels])
        batch_offsets = torch.reshape(
            torch.arange(batch_size) * height * width, [batch_size, 1])

        # This wraps array_ops.gather. We reshape the image data such that the
        # batch, y, and x coordinates are pulled into the first dimension.
        # Then we gather. Finally, we reshape the output back. It's possible this
        # code would be made simpler by using array_ops.gather_nd.
        def gather(y_coords, x_coords, name):
            linear_coordinates = batch_offsets + y_coords * width + x_coords
            gathered_values = torch.gather(flattened_grid.t(), 1, linear_coordinates)
            return torch.reshape(gathered_values,
                                 [batch_size, num_queries, channels])

        # grab the pixel values in the 4 corners around each query point
        top_left = gather(floors[0], floors[1], 'top_left')
        top_right = gather(floors[0], ceils[1], 'top_right')
        bottom_left = gather(ceils[0], floors[1], 'bottom_left')
        bottom_right = gather(ceils[0], ceils[1], 'bottom_right')

        interp_top = alphas[1] * (top_right - top_left) + top_left
        interp_bottom = alphas[1] * (bottom_right - bottom_left) + bottom_left
        interp = alphas[0] * (interp_bottom - interp_top) + interp_top

        return interp


class NoiseInject:
    """
    Generates noise samples from long noise audio using superposition.
    Combines mel spectrograms of audio and noise
    - random noise volume
    - added in random moment
    - random noise length
    - uses one of the generated samples chosen randomly for each sample
    """
    def __init__(self, mel_noise_samples, max_noise_level, to_spectrogram):
        self.mel_noise_samples = mel_noise_samples
        self.max_noise_level = max_noise_level
        self.to_spectrogram = to_spectrogram

    def __call__(self, mel_spectrogram):
        noise_spec = self.get_rdm_noise()
        weight = self.get_loudness()

        random_filled_noise = self.get_random_noise_part(mel_spectrogram, noise_spec)
        noised_mel_spec = mel_spectrogram + weight * random_filled_noise
        return noised_mel_spec

    def get_random_noise_part(self, mel_spectrogram, noise_spec):
        signal_length = mel_spectrogram.shape[-1]
        noise_length = noise_spec.shape[-1]

        random_filled_noise = torch.zeros_like(mel_spectrogram)

        max_signal_idx = torch.randint(1, signal_length, (1,))
        min_signal_idx = torch.randint(0, max_signal_idx.item(), (1,))
        rdm_len = max_signal_idx - min_signal_idx

        max_noise_idx = torch.randint(rdm_len.item(), noise_length, (1,))
        min_noise_idx = max_noise_idx - rdm_len

        random_filled_noise[:, :, min_signal_idx:max_signal_idx] = noise_spec[:, :, min_noise_idx:max_noise_idx]
        return random_filled_noise

    def get_rdm_noise(self):
        sample_nb = torch.randint(0, len(self.mel_noise_samples) - 1, (1,))
        return self.mel_noise_samples[sample_nb]

    def get_loudness(self):
        return torch.rand([1]) * self.max_noise_level

    @staticmethod
    def get_noise_combinations(noise, num_splits, combinations_num, to_spectrogram, superimpose):
        noise_samples = torch.split(noise, int(noise.shape[-1] / num_splits), dim=1)[:-1]
        if superimpose:
            noise_samples = torch.cat(noise_samples)
            assert noise_samples.shape[0] == num_splits - 1, "Wrong split in noise inj"
            mel_noise_combinations = []
            for i in range(combinations_num):
                sample_weights = F.softmax(torch.rand(len(noise_samples)), dim=0)
                superimposed_noise = torch.sum(noise_samples * sample_weights.reshape(-1, 1), dim=0)
                superimposed_noise = torch.unsqueeze(superimposed_noise, 0)
                mel_noise = to_spectrogram(superimposed_noise)
                mel_noise_combinations.append(mel_noise)
        else:
            mel_noise_combinations = [to_spectrogram(x) for x in noise_samples]
        return mel_noise_combinations

    @classmethod
    def from_file(cls, noise_file_paths, to_spectrogram, superimpose_noise):
        mel_noise_samples = []
        for single_noise_path in noise_file_paths:
            noise, _ = utilities.load_audio(single_noise_path)
            samples = cls.get_noise_combinations(
                noise=noise,
                num_splits=config.noise_signal_splits,
                combinations_num=config.num_noise_samples,
                to_spectrogram=to_spectrogram,
                superimpose=superimpose_noise
            )
            mel_noise_samples.extend(samples)

        return cls(
            mel_noise_samples=mel_noise_samples,
            max_noise_level=config.max_noise_level,
            to_spectrogram=to_spectrogram,
        )


def get_batch_fn(prediction_mode, suffix):
    def batch_audio_padding(batch):
        indexes, samples, targets = zip(*batch)
        if prediction_mode == "cls":
            targets = torch.stack(targets).squeeze(1)
        else:
            targets_shapes = torch.LongTensor([t.shape[-1] for t in targets])
            targets_padded = pad_sequence(targets, batch_first=True, padding_value=config.pad_idx)
            targets = (targets_padded, targets_shapes)

        if suffix == ".mel_spec":
            max_len = max(max([s.shape[-1] for s in samples]), config.min_padded_spectrogram_length)
            samples_padded = [torch.nn.functional.pad(
                spec, [0, max_len - spec.shape[-1]], "constant", config.pad_idx)
                for spec in samples
            ]
            samples_padded = torch.stack(samples_padded)
        elif suffix == ".wav":
            wavs = [torch.FloatTensor(wav).squeeze(0) for wav in samples]
            samples_padded = pad_sequence(wavs, batch_first=True, padding_value=0)
        else:
            raise ValueError(f"Dataset with suffix {suffix} is not supported")

        indexes = torch.LongTensor(indexes)
        return indexes, samples_padded, targets
    return batch_audio_padding


def get_pred_batch_fn(suffix):
    def batch_audio_padding(batch):
        indexes, samples = zip(*batch)

        if suffix == ".mel_spec":
            max_len = max(max([s.shape[-1] for s in samples]), config.min_padded_spectrogram_length)
            samples_padded = [torch.nn.functional.pad(
                spec, [0, max_len - spec.shape[-1]], "constant", config.pad_idx)
                for spec in samples
            ]
            samples_padded = torch.stack(samples_padded)
        elif suffix == ".wav":
            wavs = [torch.FloatTensor(wav).squeeze(0) for wav in samples]
            samples_padded = pad_sequence(wavs, batch_first=True, padding_value=0)
        else:
            raise ValueError(f"Dataset with suffix {suffix} is not supported")

        indexes = torch.LongTensor(indexes)
        return indexes, samples_padded
    return batch_audio_padding

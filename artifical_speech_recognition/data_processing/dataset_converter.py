"""Convert all wavs in dataset into mel-spectrograms and save them in analogous structure"""

import os

from pathlib import Path
import shutil
import numpy as np
import torch
import torchaudio
from tqdm import tqdm

import config
from artifical_speech_recognition.utils import utilities

HASH_DIVIDER = "_nohash_"
EXCEPT_FOLDER = "_background_noise_"


class SpeechCommandsConverter:
    def __init__(
            self,
            root,
            folders,
            save_folder_prefix,
            to_spectrogram,
    ):
        self.root = root
        self.folders = folders
        self.save_folder_prefix = save_folder_prefix
        self.to_spectrogram = to_spectrogram
        self.spec_suffix = ".mel_spec"

    def create_spec_dataset(self):
        for folder_in_archive in self.folders:
            self.save_transform(folder_in_archive, suffix=".wav")

    def save_transform(self, folder, suffix):
        root = self.root / folder
        new_folder = self.save_folder_prefix + folder
        for dirpath, _, files in os.walk(root):
            for f in tqdm(files):
                new_dirpath = dirpath.replace(folder, new_folder)
                abs_path = os.path.join(dirpath, f)
                target_path = Path(os.path.join(new_dirpath, f))
                target_path.parents[0].mkdir(parents=True, exist_ok=True)
                # create folder if does not exist

                if f.endswith(suffix) and EXCEPT_FOLDER not in dirpath:
                    # load
                    audio, sample_rate = utilities.load_audio(abs_path)
                    assert sample_rate == config.dataset_parameters[config.dataset_name]['sample_rate'],\
                        f"Wrong sample rate {sample_rate} in file {abs_path}"
                    # convert to spectrogram
                    if audio.shape[-1] < config.max_wav_length:
                        audio = np.pad(
                            audio, ((0, 0), (0, (config.max_wav_length-audio.shape[-1]))), mode='linear_ramp')
                        audio = torch.from_numpy(audio)
                    spec = self.to_spectrogram(audio)
                    # update suffix
                    target_path = target_path.with_suffix(self.spec_suffix)
                    # save in correct location
                    torch.save(spec, target_path)
                else:
                    # copy into correct location
                    shutil.copy(abs_path, target_path)


class SpeechCommandsSilenceConverter:
    def __init__(
            self,
            root,
            folder,
            save_folder_prefix,
            target_suffix,
            to_spectrogram=None,
            suffix=".wav",
            hash="_nohash_"
    ):
        self.root = root
        self.suffix = suffix
        self.folder = folder
        self.save_folder_prefix = save_folder_prefix
        self.to_spectrogram = to_spectrogram
        self.target_suffix = target_suffix
        self.suffix = suffix
        self.hash = hash

    def create_silence_class(self):
        target_root = self.root / (self.save_folder_prefix + self.folder) / "silence"
        target_root.mkdir(exist_ok=True)
        self.create_silence_examples_from_wav(target_root)
        self.generate_silent_files(target_root)

    def create_silence_examples_from_wav(self, target_root):
        source_root = self.root / self.folder / EXCEPT_FOLDER
        for dirpath, _, files in os.walk(source_root):
            for f in tqdm(files):
                if f.endswith(self.suffix):
                    source_path = os.path.join(dirpath, f)
                    processed_data = self.process_wav(source_path)

                    target_path = Path(os.path.join(target_root, f))
                    target_path.parents[0].mkdir(parents=True, exist_ok=True)
                    for idx in range(processed_data.shape[0]):
                        sample_target_path = \
                            str(target_path.parents[0]) + "/" + self.hash + target_path.name.split(".")[0] + (str(idx) + self.target_suffix)
                        if self.target_suffix == ".mel_spec":
                            torch.save(processed_data[idx].clone(), sample_target_path)
                        elif self.target_suffix == ".wav":
                            torchaudio.save(
                                filepath=str(sample_target_path),
                                src=processed_data[idx].clone(),
                                sample_rate=config.sample_rate
                            )
                        else:
                            raise ValueError(f"Target conversion suffix {self.target_suffix} is not supported")

    def process_wav(self, path):
        audio, sample_rate = utilities.load_audio(path)
        assert sample_rate == config.dataset_parameters[config.dataset_name]['sample_rate'], \
            f"Wrong sample rate {sample_rate} in file {path}"

        noise_samples = torch.split(audio, config.sample_rate, dim=1)[:-1]
        low_volume_samples = [s / 10 for s in noise_samples]
        noise_samples = torch.cat([*noise_samples] * config.silent_samples_repetition)
        samples = torch.cat([noise_samples, *low_volume_samples])
        if self.to_spectrogram:
            samples = torch.cat([self.to_spectrogram(sample) for sample in samples], dim=0)
        return samples

    def generate_silent_files(self, target_root):
        samples_nb = config.silent_subclass_samples*config.silent_samples_repetition
        silence_samples = torch.zeros((samples_nb, config.sample_rate))
        for sample_idx in range(silence_samples.shape[0]):
            sample = silence_samples[sample_idx]
            if self.to_spectrogram:
                sample = self.to_spectrogram(sample)
            path = target_root/(self.hash + "silence_sample" + str(sample_idx) + self.target_suffix)
            if self.target_suffix == ".mel_spec":
                torch.save(sample.clone(), path)
            elif self.target_suffix == ".wav":
                torchaudio.save(filepath=str(path), src=sample.clone(), sample_rate=config.sample_rate)
            else:
                raise ValueError(f"Target conversion suffix {self.target_suffix} is not supported")

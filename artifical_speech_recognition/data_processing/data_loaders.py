import os


HASH_DIVIDER = "_nohash_"
EXCEPT_FOLDER = "_background_noise_"


loader_types = {}


def register(loader):
    loader_types[loader.__name__] = loader
    return loader


@register
class SpeechCommandsLoader:
    def __init__(
            self,
            root,
            folders,
            valid_file_names,
            test_file_names,
            suffix,
    ):
        self.root = root
        self.folders = folders
        self.valid_file_names = valid_file_names
        self.test_file_names = test_file_names
        self.suffix = suffix
        self.init_splits()

    def get_splits_files(self):
        for folder_in_archive in self.folders:
            path = os.path.join(self.root, folder_in_archive)
            self.walk_files(path, suffix=self.suffix)

        splits = self.get_data_dict()
        self.init_splits()
        return splits

    def walk_files(self, root, suffix):
        root = os.path.expanduser(root)
        for dirpath, _, files in os.walk(root):
            for f in files:
                if f.endswith(suffix) and HASH_DIVIDER in f and EXCEPT_FOLDER not in dirpath:
                    folder = os.path.basename(dirpath)
                    name = os.path.join(folder, f)
                    f = os.path.join(dirpath, f)
                    label = os.path.basename(dirpath)
                    if name in self.valid_file_names:
                        self.valid_files.append((label, f))
                    elif name in self.test_file_names:
                        self.test_files.append((label, f))
                    else:
                        self.train_files.append((label, f))

    def get_data_dict(self):
        return {
            "train_ds": self.train_files,
            "ordered_train_ds": self.train_files,
            "valid_ds": self.valid_files,
            "test_ds": self.test_files,
        }

    def init_splits(self):
        self.train_files = []
        self.valid_files = []
        self.test_files = []


@register
class LJSpeechLoader:
    def __init__(
            self,
            root,
            folders,
            labels_file_names,
            valid_file_names,
            test_file_names,
            suffix,
    ):
        self.root = root
        self.folders = folders
        self.valid_file_names = valid_file_names
        self.test_file_names = test_file_names
        self.labels_file_name = labels_file_names # set first column as index
        self.suffix = suffix
        self.init_splits()

    def get_splits_files(self):
        for folder_in_archive in self.folders:
            path = os.path.join(self.root, folder_in_archive)
            self.walk_files(path, suffix=self.suffix)

        splits = self.get_data_dict()
        self.init_splits()
        return splits

    def walk_files(self, root, suffix):
        root = os.path.expanduser(root)
        for dirpath, _, files in os.walk(root):
            for f in files:
                if f.endswith(suffix) and HASH_DIVIDER in f and EXCEPT_FOLDER not in dirpath:
                    folder = os.path.basename(dirpath)
                    name = os.path.join(folder, f)
                    f = os.path.join(dirpath, f)
                    label = self.labels_file_name.loc[f]
                    if name in self.valid_file_names:
                        self.valid_files.append((label, f))
                    elif name in self.test_file_names:
                        self.test_files.append((label, f))
                    else:
                        self.train_files.append((label, f))

    def get_data_dict(self):
        return {
            "train_ds": self.train_files,
            "valid_ds": self.valid_files,
            "test_ds": self.test_files,
        }

    def init_splits(self):
        self.train_files = []
        self.valid_files = []
        self.test_files = []


class SubmissionLoader:
    def __init__(
            self,
            root,
            folders,
            suffix='.wav',
    ):
        self.root = root
        self.folders = folders
        self.suffix = suffix

    def get_files(self):
        wav_files = []
        for folder in self.folders:
            root = os.path.join(self.root, folder)
            root = os.path.expanduser(root)
            for dirpath, _, files in os.walk(root):
                for f in files:
                    if f.endswith(self.suffix):
                        f = os.path.join(dirpath, f)
                        label = os.path.basename(dirpath)
                        wav_files.append((label, f))

        return wav_files


def get_data_loader_cls(ds_name):
    for supported_loader in loader_types.keys():
        name = supported_loader.replace("Loader", "")
        if name in ds_name:
            return loader_types[supported_loader]

    raise ValueError(f'{ds_name} is not a valid dataset option. Supported loader types {loader_types.keys()}')

import copy
import collections

import torch
from torch.utils.data import dataloader

from artifical_speech_recognition.data_processing import transforms
from artifical_speech_recognition.data_processing import dataset as ds
from artifical_speech_recognition.data_processing import text_processing
from artifical_speech_recognition.data_processing import data_loaders
from artifical_speech_recognition.utils import utilities
import config


class DataBunch:
    @staticmethod
    def to_dataloader(dataset, collate_fn, sampler=None, shuffle=True):
        if sampler:
            dl = dataloader.DataLoader(
                dataset=dataset,
                sampler=sampler,
                collate_fn=collate_fn,
                batch_size=config.bs,
                num_workers=config.loader_workers,
            )
        else:
            dl = dataloader.DataLoader(
                dataset=dataset,
                shuffle=shuffle,
                batch_size=config.bs,
                collate_fn=collate_fn,
                num_workers=config.loader_workers,
            )
        return dl


class TrainingDataBunch(DataBunch):
    def __init__(self, train_ds, ordered_train_ds=None, test_ds=None, valid_ds=None, collate_fn=None, sampler=None):
        self.train = self.to_dataloader(train_ds, collate_fn, sampler=sampler, shuffle=True)
        self.ordered_train = self.to_dataloader(ordered_train_ds, collate_fn, shuffle=False)
        self.valid = self.to_dataloader(valid_ds, collate_fn, shuffle=False) if valid_ds else []
        self.test = self.to_dataloader(test_ds, collate_fn, shuffle=False) if test_ds else []

    def __getitem__(self, mode):
        return self.__dict__[mode]

    def get_vocab_size(self):
        return self.train.dataset.get_vocab_size()

    def textify(self, *samples):
        return [self.train.dataset.textify(nums) for nums in samples]

    def get_dataset_transformations(self):
        transformations = {
            "train_ds": {
                "augmentation": self.train.dataset.augmentation,
            },
            "ordered_train_ds": {
                "augmentation": self.valid.dataset.augmentation,
            },
            "valid_ds": {
                "augmentation": self.valid.dataset.augmentation,
            },
            "test_ds": {
                "augmentation": self.test.dataset.augmentation,
            }
        }
        assert set(config.dataset_types.keys()) == set(transformations.keys())\
            , "saving checkpoint and config have incompatible dataset splits"
        return transformations

    @classmethod
    def get_sets_augmentation_transforms(cls, dataset_types, to_spectrogram, suffix):
        return {ds_type: cls.get_augmentation_transform(ds_type, to_spectrogram, suffix) for ds_type in dataset_types}

    @staticmethod
    def get_augmentation_transform(ds_type, to_spectrogram, suffix):
        folder = config.dataset_parameters[config.dataset_name]['folders'][0]
        noise_files = config.dataset_parameters[config.dataset_name]['noise_file_path']
        noise_file_paths = [config.data_path/folder/noise_file for noise_file in noise_files]

        if suffix == ".mel_spec":
            if ds_type == "train":
                augmentation = transforms.Compose([
                    transforms.MinMaxScale(
                        mean=config.dataset_parameters[config.dataset_name]["mean"],
                    ),
                    transforms.NoiseInject.from_file(
                        noise_file_paths=noise_file_paths,
                        to_spectrogram=to_spectrogram if to_spectrogram else transforms.get_spectrogram_transform(),
                        superimpose_noise=config.superimpose_noise,
                    ),
                    # transforms.WarpTime(speed=config.time_warping_para),
                    transforms.MaskSpectrogramFreq(max_mask_len=config.freq_mask_max_len),
                    transforms.MaskSpectrogramTime(max_mask_len=config.time_mask_max_len),
                ])
            else:
                augmentation = transforms.Compose([
                    transforms.MinMaxScale(
                        mean=config.dataset_parameters[config.dataset_name]["mean"],
                    )
                ])
        elif suffix == ".wav":
            if ds_type == "train":
                augmentation = transforms.Compose([
                    transforms.ToDecibel(
                        mean=config.dataset_parameters[config.dataset_name]["mean"],
                    ),
                    transforms.WavNoiseInject.from_file(noise_file_paths=noise_file_paths),
                    # transforms.WavEdgeZeroing(
                    #     proportion=config.zeroing_proportion,
                    #     transformation_prob=config.augmentation_probability
                    # ),
                    # timestrech # resample by random small value and cut to same size?
                ])
            else:
                augmentation = transforms.Compose([
                    transforms.ToDecibel(
                        mean=config.dataset_parameters[config.dataset_name]["mean"],
                    )
                ])
        else:
            raise ValueError(f"Default transforms for suffix {suffix} are not supproted")

        return augmentation

    @staticmethod
    def get_split_files(folders):
        folder = folders[0]
        valid_file = config.dataset_parameters[config.dataset_name]['valid_split_path']
        test_file = config.dataset_parameters[config.dataset_name]['test_split_path']
        valid_split_path = config.data_path/folder/valid_file
        test_split_path = config.data_path/folder/test_file
        valid_file_names = utilities.load_txt_to_df(valid_split_path)
        test_file_names = utilities.load_txt_to_df(test_split_path)
        return valid_file_names, test_file_names

    @staticmethod
    def load_data_path_splits(suffix):
        folders = config.dataset_parameters[config.dataset_name]['folders']
        valid_file_names, test_file_names = TrainingDataBunch.get_split_files(folders)

        data_loader_cls = data_loaders.get_data_loader_cls(config.dataset_name)
        data_loader = data_loader_cls(
            root=config.data_path,
            folders=folders,
            valid_file_names=valid_file_names,
            test_file_names=test_file_names,
            suffix=suffix,
        )
        return data_loader.get_splits_files()

    @classmethod
    def get_class_weights(cls, file_paths):
        label_mapping = utilities.get_label_mapping(config.label_mapping_type)
        labels = [label_mapping(x[0]) for x in file_paths]
        label_counts = collections.Counter(labels)
        weights = [1/label_counts[k] for k in sorted(label_counts.keys())]
        return torch.FloatTensor(weights) / sum(weights)

    @classmethod
    def get_sampler(cls, file_paths, label_mapping):
        weights = cls.get_class_weights(file_paths=file_paths)
        sample_weights = [weights[label_mapping(x[0])].item() for x in file_paths]
        sampler = torch.utils.data.sampler.WeightedRandomSampler(
            weights=sample_weights,
            num_samples=len(file_paths),
            replacement=True,
        )
        return sampler

    @classmethod
    def get_dataset_classes(cls, prediction_mode, suffix):
        train_ds = ds.get_dataset(prediction_mode=prediction_mode, suffix=suffix, label_mix=config.label_mix)
        other_ds = ds.get_dataset(prediction_mode=prediction_mode, suffix=suffix, label_mix=False)
        return {"train_ds": train_ds, "ordered_train_ds": other_ds, "test_ds": other_ds, "valid_ds": other_ds}

    @classmethod
    def from_parameters(cls, prediction_mode, augmentation=None, to_spectrogram=None):
        dataset_types = copy.deepcopy(config.dataset_types)
        suffix = config.dataset_parameters[config.dataset_name]['suffix']
        dataset_classes = cls.get_dataset_classes(prediction_mode=prediction_mode, suffix=suffix)
        file_paths = cls.load_data_path_splits(suffix)
        label_mapping = utilities.get_label_mapping(config.label_mapping_type)
        sampler = cls.get_sampler(file_paths=file_paths['train_ds'], label_mapping=label_mapping)
        augmentation = augmentation if augmentation else cls.get_sets_augmentation_transforms(
            dataset_types=dataset_types,
            to_spectrogram=to_spectrogram,
            suffix=suffix
        )

        datasets = {
            ds_type: dataset_classes[ds_type](
                file_paths=file_paths[ds_type],
                augmentation=augmentation[ds_type],
                vocab=text_processing.CharacterVocab.from_ascii(),
                label_mapping=label_mapping,
                sampler=sampler,
            )
            for ds_type in dataset_types
        }
        return cls(
            collate_fn=transforms.get_batch_fn(prediction_mode, suffix),
            sampler=sampler,
            **datasets
        )

    @classmethod
    def from_config(cls, prediction_mode, to_spectrogram=None):
        suffix = config.dataset_parameters[config.dataset_name]['suffix']
        file_paths = cls.load_data_path_splits(suffix=suffix)

        dataset_classes = cls.get_dataset_classes(prediction_mode=prediction_mode, suffix=suffix)
        label_mapping = utilities.get_label_mapping(config.label_mapping_type)
        sampler = cls.get_sampler(file_paths=file_paths['train_ds'], label_mapping=label_mapping)

        augmentation = cls.get_sets_augmentation_transforms(
            dataset_types=config.dataset_types,
            to_spectrogram=to_spectrogram,
            suffix=suffix
        )

        datasets = {
            ds_type: dataset_classes[ds_type](
                file_paths=file_paths[ds_type],
                augmentation=augmentation[ds_type],
                vocab=text_processing.CharacterVocab.from_ascii(),
                label_mapping=label_mapping,
                sampler=sampler,
            ) for ds_type in config.dataset_types
        }
        return cls(
            collate_fn=transforms.get_batch_fn(prediction_mode=prediction_mode, suffix=suffix),
            sampler=sampler,
            **datasets
        )


class PredictionDataBunch(DataBunch):
    def __init__(self, datasets, file_names, collate_fn=None):
        self.datasets = [self.to_dataloader(data, collate_fn, shuffle=False) for data in datasets]
        self.file_names = file_names

    @staticmethod
    def get_audio_paths():
        dl = data_loaders.SubmissionLoader(root=config.submission_wavs_root, folders=config.submission_wavs_folders)
        wav_files = dl.get_files()
        return wav_files

    @staticmethod
    def get_augmentation_transforms():
        augmentation = transforms.Compose([
                          transforms.MinMaxScale(
                              mean=config.dataset_parameters[config.dataset_name]["mean"],
                          )])
        return augmentation

    @classmethod
    def from_config(cls, suffix, tta_iter, augmentation=None, to_spectrogram=None):
        audio_paths = cls.get_audio_paths()
        if suffix == ".mel_spec":
            to_spectrogram = to_spectrogram if to_spectrogram else transforms.get_spectrogram_transform()
            augmentation = augmentation if augmentation else cls.get_augmentation_transforms()
        elif suffix == ".wav":
            to_spectrogram = None
        else:
            raise ValueError(f"Suffix {suffix} not supported in prediciton pipeline")

        datasets = [ds.AudioDataset(
            wav_paths=audio_paths,
            to_spectrogram=to_spectrogram,
            augmentation=augmentation,
            vocab=text_processing.CharacterVocab.from_ascii(),
        ) for _ in range(tta_iter)]

        return cls(datasets=datasets, file_names=audio_paths, collate_fn=transforms.get_pred_batch_fn(suffix))

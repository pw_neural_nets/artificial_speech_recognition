from types import ModuleType

from fuzzywuzzy import fuzz
import numpy as np
import torch
import random
import librosa

import config

GPU_NAME = "cuda:0"
CPU_NAME = "cpu"


def get_device():
    return torch.device(GPU_NAME if torch.cuda.is_available() else CPU_NAME)


def set_random_seed(seed_value=config.SEED):
    np.random.seed(seed_value)
    torch.manual_seed(seed_value)
    random.seed(seed_value)
    if get_device() == GPU_NAME:
        torch.cuda.manual_seed(seed_value)
        torch.cuda.manual_seed_all(seed_value)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False


def get_config_dict():
    return {k: v for k, v in config.__dict__.items()
            if not k.startswith("__") and not isinstance(v, ModuleType)}


def load_txt_to_df(path):
    with open(path, 'r') as f:
        strings = set([w.strip() for w in f.readlines()])
    return strings


def get_most_similar_label(pred_word):
    return max([(x, fuzz.ratio(x, pred_word)) for x in config.all_labels], key=lambda x: x[1])[0]


def get_label_to_unk_labels_mapping():
    label_mapping = {}
    unk_label = len(config.all_labels) - 1
    for idx in range(len(config.all_labels)):
        if config.all_labels[idx] in config.target_labels:
            label_mapping[idx] = idx
        else:
            label_mapping[idx] = unk_label
    return label_mapping


def get_label_mapping(label_mapping_type):
    if label_mapping_type == "unk":
        def label_mapping(label):
            if label in config.target_labels:
                return config.target_labels.index(label)
            else:
                return config.target_labels.index("unknown")
        return label_mapping
    else:
        return config.all_labels.index


def idx_to_label(pred):
    return config.target_labels[pred]


def load_audio(path):
    sig, rate = librosa.load(path, sr=config.sample_rate)
    if len(sig) < config.max_wav_length:  # pad shorter than 1 sec audio with ramp to zero
        sig = np.pad(sig, (0, config.max_wav_length - len(sig)), 'linear_ramp')[:16000]
    return torch.from_numpy(sig).unsqueeze(0), rate

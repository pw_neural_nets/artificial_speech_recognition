import math

import torch
from torch import nn

from artifical_speech_recognition.model.utils import activation
import config


# TODO
# - probably will need some stronger regularization
# - check lstm vs rnn
# - think how do i initialize lstm hidden state


class DeepSpeechBlock(nn.Module):
    def __init__(self, in_features, out_features, p, max_value=config.max_truncated_relu):
        super().__init__()
        self.fc = nn.Linear(in_features=in_features, out_features=out_features)
        self.trelu = activation.TruncatedRelu(max_value)
        self.dropout = nn.Dropout(p=p)

    def forward(self, x):
        x = self.fc(x)
        x = self.trelu(x)
        x = self.dropout(x)
        return x


class DeepSpeech(nn.Module):
    """
    Vannila:
    Deep Speech 1: https://arxiv.org/abs/1412.5567v2

    For efficiency uses RNN without lstm gateways
    Big dataset used as regularization
    Dropout 0.05 to 0.10 only in feedforward layers
    Nesterov momentum
    """
    def __init__(self, in_features, hidden_fc, vocab_size, dropout):
        super().__init__()
        block2_hs = hidden_fc // 2
        block3_hs = hidden_fc // 4
        hidden_lstm = 2

        self.lstm = nn.LSTM(
            input_size=hidden_lstm,
            hidden_size=hidden_lstm,
            bidirectional=True,
        )
        self.block1 = DeepSpeechBlock(in_features=in_features, out_features=hidden_fc, p=dropout)
        self.block2 = DeepSpeechBlock(in_features=hidden_fc, out_features=block2_hs, p=dropout)
        self.block3 = DeepSpeechBlock(in_features=block2_hs, out_features=block3_hs, p=dropout)
        self.vocab_fc = nn.Linear(in_features=hidden_lstm, out_features=vocab_size)
        self.softmax = nn.Softmax(dim=1)
        self.vocab_dropout = nn.Dropout(p=dropout)

    def forward(self, spectrograms):
        x = self.trelu(self.fc1(spectrograms))
        x = self.trelu(self.fc2(x))
        x = self.trelu(self.fc3(x))
        x, state = self.lstm(x)
        x = self.vocab_dropout(self.fc5(x))
        character_probs = self.softmax(x)
        return character_probs

    @classmethod
    def from_config(cls, vocab_size):
        return cls(
            in_features=config.ds_in_featuers,
            hidden_fc=config.ds_hidden_fc,
            vocab_size=vocab_size,
            dropout=config.ds_dropout,
        )


class DeepSpeech2Block(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride,
                 padding, dropout, max_value=config.max_truncated_relu):
        super().__init__()
        self.conv = nn.Conv2d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=kernel_size,
            stride=stride,
            padding=padding,
        )
        self.bn = nn.BatchNorm2d(out_channels)
        self.trelu = activation.TruncatedRelu(max_value)
        self.dropout = nn.Dropout(p=dropout)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = self.trelu(x)
        x = self.dropout(x)
        return x


class DeepSpeech2(nn.Module):
    """Deep Speech 2: https://arxiv.org/abs/1512.02595v1

    split spectrogram into pieces? window moving on spectrogram?
    encoder-decoder with attention (fixed length vector)
    conv -> rnn
    optimization with SGD
    """
    def __init__(self, channels_shapes, vocab_size, vocab_dropout,
                 time_kernel_size, time_stride, time_padding, spec_kernel_size,
                 spec_stride, spec_padding, conv_dropout, rnn_dropout):
        super().__init__()
        ins, outs = channels_shapes[:-1], channels_shapes[1:]

        hidden_lstm = config.n_mels
        for skernel_size, sstride, spadding in zip(spec_kernel_size, spec_stride, spec_padding):
            hidden_lstm = int(math.floor(hidden_lstm + 2 * spadding - skernel_size) / sstride + 1)
        hidden_lstm *= outs[-1]
        hidden_size_lstm = hidden_lstm * 2 if config.bidirectional else hidden_lstm

        self.layers = nn.ModuleList([
            DeepSpeech2Block(
                in_channels=input_shape,
                out_channels=output_shape,
                kernel_size=(skernel_size, tkernel_size),
                stride=(sstride, tstride),
                padding=(spadding, tpadding),
                dropout=conv_dropout,
            )

            for input_shape, output_shape, tkernel_size, tstride, tpadding, skernel_size, sstride, spadding in
            zip(ins, outs, time_kernel_size, time_stride, time_padding, spec_kernel_size, spec_stride, spec_padding)
        ])

        self.lstm = nn.LSTM(
            input_size=hidden_size_lstm,
            hidden_size=hidden_size_lstm,
            bidirectional=config.bidirectional,
            num_layers=config.lstm_layers,
            dropout=rnn_dropout,
        )
        self.vocab_fc = nn.Linear(in_features=hidden_size_lstm, out_features=vocab_size)
        self.vocab_dropout = nn.Dropout(p=vocab_dropout)

    def forward(self, x):
        # local attention?
        for layer in self.layers:
            x = layer(x)
        bs, filters, spec, time = x.shape
        x = x.view(bs, -1, time)
        x = x.permute(2, 0, 1)
        x, state = self.lstm(x)
        x = x.permute(1, 0, 2)
        x = self.vocab_dropout(self.vocab_fc(x))
        character_probs = x.log_softmax(2).permute(1, 0, 2)
        return character_probs

    def get_lstm_size(self):
        return self.lstm.hidden_size

    @classmethod
    def from_config(cls, vocab_size):
        return cls(
            channels_shapes=config.channels_shapes,
            time_kernel_size=config.time_kernel_size,
            time_stride=config.time_stride,
            time_padding=config.time_padding,
            spec_kernel_size=config.spec_kernel_size,
            spec_stride=config.spec_stride,
            spec_padding=config.spec_padding,
            vocab_size=vocab_size,
            vocab_dropout=config.fc_dropout,
            conv_dropout=config.conv_dropout,
            rnn_dropout=config.rnn_dropout,
        )


class SpeechAttention(nn.Module):
    def __init__(self, in_features, dropout):
        super().__init__()
        self.fc = nn.Linear(in_features, 1)
        self.softmax = nn.Softmax(dim=1)
        self.dropout = nn.Dropout(p=dropout)

    def forward(self, x): # bs, seq, hidden
        x = self.dropout(x)
        weights = self.fc(x) # bs, seq, 1
        weights = self.softmax(weights) # bs, seq, 1
        x = x * weights # bs, seq, hidden
        x = x.sum(dim=1) # bs, hidden
        # potential fc(x) # bs, hidden -> bs, attn_dim
        return x


class AttentionClassifier(nn.Module):
    def __init__(self, channels_shapes, time_kernel_size, time_stride, time_padding, spec_kernel_size,
                 spec_stride, spec_padding, conv_dropout, rnn_dropout, label_mapping):
        super().__init__()
        ins, outs = channels_shapes[:-1], channels_shapes[1:]

        hidden_lstm = config.n_mels
        for skernel_size, sstride, spadding in zip(spec_kernel_size, spec_stride, spec_padding):
            hidden_lstm = int(math.floor(hidden_lstm + 2 * spadding - skernel_size) / sstride + 1)
        hidden_lstm *= outs[-1]
        hidden_size_lstm = hidden_lstm * 2 if config.bidirectional else hidden_lstm

        self.layers = nn.ModuleList([
            DeepSpeech2Block(
                in_channels=input_shape,
                out_channels=output_shape,
                kernel_size=(skernel_size, tkernel_size),
                stride=(sstride, tstride),
                padding=(spadding, tpadding),
                dropout=conv_dropout,
            )

            for input_shape, output_shape, tkernel_size, tstride, tpadding, skernel_size, sstride, spadding in
            zip(ins, outs, time_kernel_size, time_stride, time_padding, spec_kernel_size, spec_stride, spec_padding)
        ])

        self.lstm = nn.LSTM(
            input_size=hidden_lstm,
            hidden_size=hidden_lstm,
            bidirectional=config.bidirectional,
            num_layers=config.lstm_layers,
            dropout=rnn_dropout,
        )

        self.attention_layer = SpeechAttention(in_features=hidden_size_lstm, dropout=config.attn_dropout)
        self.fc_cls = nn.Linear(in_features=hidden_size_lstm, out_features=len(label_mapping))
        self.fc_dropout = nn.Dropout(p=config.fc_dropout)
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        bs, filters, spec, time = x.shape
        x = x.view(bs, -1, time)
        x = x.permute(2, 0, 1)
        x, state = self.lstm(x)
        x = x.permute(1, 0, 2)
        x = self.attention_layer(x)
        x = self.fc_dropout(x)
        class_probs = self.softmax(self.fc_cls(x))
        return class_probs

    def get_lstm_size(self):
        return self.lstm.hidden_size

    @classmethod
    def from_config(cls, label_mapping):
        return cls(
            channels_shapes=config.channels_shapes,
            time_kernel_size=config.time_kernel_size,
            time_stride=config.time_stride,
            time_padding=config.time_padding,
            spec_kernel_size=config.spec_kernel_size,
            spec_stride=config.spec_stride,
            spec_padding=config.spec_padding,
            conv_dropout=config.conv_dropout,
            rnn_dropout=config.rnn_dropout,
            label_mapping=label_mapping,
        )


class Conv1dBlock(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride, dilation, padding):
        super().__init__()
        self.conv = nn.Conv1d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=kernel_size,
            stride=stride,
            dilation=dilation,
            padding=padding
        )
        self.bn = nn.BatchNorm1d(out_channels)
        self.relu = nn.ReLU()
        # self.dropout = nn.Dropout(p=dropout)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = self.relu(x)
        return x


class WavClassifier(nn.Module):
    def __init__(self, channels_shapes, kernels_size, strides, dilations, paddings, lstm_shape, label_mapping):
        super().__init__()
        ins, outs = channels_shapes[:-1], channels_shapes[1:]

        self.layers = nn.ModuleList([
            Conv1dBlock(
                in_channels=input_shape,
                out_channels=output_shape,
                kernel_size=kernel_size,
                stride=stride,
                dilation=dilation,
                padding=padding
            )

            for input_shape, output_shape, kernel_size, stride, dilation, padding in
            zip(ins, outs, kernels_size, strides, dilations, paddings)
        ])

        self.lstm = nn.LSTM(
            input_size=lstm_shape,
            hidden_size=lstm_shape,
            num_layers=config.lstm_layers,
        )

        self.attention_layer = SpeechAttention(in_features=lstm_shape, dropout=config.attn_dropout)
        self.fc_cls = nn.Linear(in_features=lstm_shape, out_features=len(label_mapping))
        self.fc_dropout = nn.Dropout(p=config.fc_dropout)
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        x = x.unsqueeze(1)
        for layer in self.layers:
            x = layer(x)
        x = x.permute(2, 0, 1)
        x, state = self.lstm(x)
        x = x.permute(1, 0, 2)
        x = self.attention_layer(x)
        x = self.fc_dropout(x)
        class_probs = self.softmax(self.fc_cls(x))
        return class_probs

    def get_lstm_size(self):
        return self.lstm.hidden_size

    @classmethod
    def from_config(cls, label_mapping):
        return cls(
            channels_shapes=config.wav_channels_shapes,
            kernels_size=config.wav_kernel_size,
            strides=config.wav_stride_size,
            dilations=config.wav_dilatation_size,
            paddings=config.wav_padding_size,
            lstm_shape=config.lstm_shape,
            label_mapping=label_mapping,
        )


class WavDIYSpecClassifier(nn.Module):
    def __init__(
            self, channels1d_shapes, channels2d_shapes, kernels_size, strides1d, dilations1d, paddings1d,
            time_kernel_size, time_stride, time_padding, spec_kernel_size, spec_stride, spec_padding,
            lstm_shape, label_mapping
    ):
        super().__init__()
        ins1d, outs1d = channels1d_shapes[:-1], channels1d_shapes[1:]
        ins2d, outs2d = channels2d_shapes[:-1], channels2d_shapes[1:]

        self.layers1d = nn.ModuleList([
            Conv1dBlock(
                in_channels=input_shape,
                out_channels=output_shape,
                kernel_size=kernel_size,
                stride=stride,
                dilation=dilation,
                padding=padding
            )
            for input_shape, output_shape, kernel_size, stride, dilation, padding in
            zip(ins1d, outs1d, kernels_size, strides1d, dilations1d, paddings1d)
        ])

        self.layers2d = nn.ModuleList([
            DeepSpeech2Block(
                in_channels=input_shape,
                out_channels=output_shape,
                kernel_size=(skernel_size, tkernel_size),
                stride=(sstride, tstride),
                padding=(spadding, tpadding),
                dropout=0,
            )

            for input_shape, output_shape, tkernel_size, tstride, tpadding, skernel_size, sstride, spadding in
            zip(ins2d, outs2d, time_kernel_size, time_stride, time_padding, spec_kernel_size, spec_stride, spec_padding)
        ])

        self.lstm = nn.LSTM(
            input_size=lstm_shape,
            hidden_size=lstm_shape,
            num_layers=config.lstm_layers,
        )

        self.attention_layer = SpeechAttention(in_features=lstm_shape, dropout=config.attn_dropout)
        self.fc_cls = nn.Linear(in_features=lstm_shape, out_features=len(label_mapping))
        self.fc_dropout = nn.Dropout(p=config.fc_dropout)
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        x = x.unsqueeze(1)
        for layer in self.layers1d:
            x = layer(x)

        x = x.unsqueeze(1)

        for layer in self.layers2d:
            x = layer(x)

        bs, filters, spec, time = x.shape
        x = x.view(bs, -1, time)
        x = x.permute(2, 0, 1)
        x, state = self.lstm(x)
        x = x.permute(1, 0, 2)
        x = self.attention_layer(x)
        x = self.fc_dropout(x)
        class_probs = self.softmax(self.fc_cls(x))
        return class_probs

    def get_lstm_size(self):
        return self.lstm.hidden_size

    @classmethod
    def from_config(cls, label_mapping):
        return cls(
            channels1d_shapes=[1, 60, 80, 120],
            channels2d_shapes=[1, 10, 16],
            kernels_size=[80, 60, 40],
            strides1d=[4, 4, 4],
            dilations1d=[2, 2, 1],
            paddings1d=[10, 10, 10],
            time_kernel_size=[11, 11],
            time_stride=[4, 2],
            time_padding=[10, 10],
            spec_kernel_size=[35, 30],
            spec_stride=[3, 2],
            spec_padding=[20, 25],
            lstm_shape=512,
            label_mapping=label_mapping,
        )

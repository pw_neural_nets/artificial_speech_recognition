import torch
from torch import nn


class TruncatedRelu(nn.Module):
    def __init__(self, maximal_value):
        super().__init__()
        self.maximal_value = maximal_value
        self.relu = nn.ReLU()

    def forward(self, x):
        x = self.relu(x)
        x = torch.clamp(x, max=self.maximal_value)
        return x

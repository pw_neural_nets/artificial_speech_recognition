## Ideas to check

#### Models

- <strong>train sub model that is trained only on classes that main model struggles on and then if main model predicts one of this classes sub model is run to test which one is it</strong>
- triplet loss to handle unknown class (seems like an overkill for 10% of dataset)

#### Ensemble

- <strong>train variants of the model on wav, mel-spec</strong>
- <strong>use probability vectors predicted by model as ensemble input (compare vs one hot  result) </strong>
- <strong>train next model on residuals of first model (gradient boosting) </strong>
- check out population based training

#### Augmentation

- <strong>mix samples two samples and set correct label to proportion of this classes</strong>
- <strong>add random zeros/random values square on mel spectrogram (random freq and time length)</strong>
- compare adding noise to all sample vs random length noise
- what performs better? Augmentation always or with some probability of happening

#### Other

- check influence of pseudo labeling (threshold and fuzzy labels)
- compare multiple runs of the same model with different seeds vs multiple runs with dropout as estimate of model variation
- dual loss for the model CTC and classification

## Models to run

- acc save comp bidir
- acc save comp mel
- acc save comp wav2d 180 (needs sub pred)
- acc save mel
- acc save wav2d
- linear sample mix (running)

## Experiments Results

- TTA with freq and time mask + random noise resulted in worse performance 0.878 to 0.86

- meta models trained on classes with unknown class: 

  - lm (worse than single model)
  
    train, acc score: 0.9002626494035428
    test, acc score:  0.8930968840797515
    valid, acc score: 0.8876874148114493
    public: 0.8548
    private: 0.85526
    
  - rf train (all data)
    train, acc score: 0.9529198051534822 
    test, acc score: 0.9346758841799419 
    valid, acc score: 0.9296683325761018
    public:  0.87143 (0.87554)
    private:  0.88006 (0.88535)
  
  - catboost (same for all data and only train)
    train, acc score: 0.9474585488331193
    valid, acc score: 0.9243071331213085
    test, acc score: 0.9306682697124536
    public: 0.87774
    private: 0.88593
  
- use every type of silence sample as the background seems to give worse results
  (validate if its true by running mel superposition on white noise and checking if results are ~0.87 instead of 0.86)
  
  
import os

import torch
from tqdm import tqdm

import config
from artifical_speech_recognition.utils import utilities


class SpectrogramMeanCalculator:
    def __init__(
            self,
            root,
            folders,
            suffix,
            exclude_folder,
    ):
        self.root = root
        self.folders = folders
        self.suffix = suffix
        self.exclude_folder = exclude_folder if exclude_folder else None
        self.means = []

    def calculate_spec_dataset_mean(self):
        for folder_in_archive in self.folders:
            if self.suffix == ".mel_spec":
                self.get_spec_mean(folder_in_archive, suffix=self.suffix)
            elif self.suffix == ".wav":
                self.get_wav_mean(folder_in_archive, suffix=self.suffix)

    def get_spec_mean(self, folder, suffix):
        root = self.root / folder
        for dirpath, _, files in os.walk(root):
            for f in tqdm(files):
                abs_path = os.path.join(dirpath, f)
                if f.endswith(suffix):# and self.exclude_folder not in dirpath:
                    spectrogram = torch.load(abs_path)
                    mean = spectrogram.mean()
                    self.means.append(mean)
        print(torch.mean(torch.stack(self.means)))

    def get_wav_mean(self, folder, suffix):
        root = self.root / folder
        for dirpath, _, files in os.walk(root):
            for f in tqdm(files):
                abs_path = os.path.join(dirpath, f)
                if f.endswith(suffix):# and self.exclude_folder not in dirpath:
                    wav, samplerate = utilities.load_audio(abs_path)
                    mean = wav.mean()
                    self.means.append(mean)
        print(torch.mean(torch.stack(self.means)))


smc = SpectrogramMeanCalculator(
    root=config.data_path,
    folders=config.dataset_parameters[config.dataset_name]["folders"],
    suffix=".wav", #".mel_spec",
    exclude_folder=None#"silence",
)

smc.calculate_spec_dataset_mean()
